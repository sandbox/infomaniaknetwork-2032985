CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * FAQ


 INTRODUCTION
------------
This plugin allows you to easily manage the interactions between your Drupal site and your VOD space. It lets you easily retrieve and manage all of your videos.

If you would like more information about our video hosting solution, please visit : http://streaming.infomaniak.com/stockage-video-en-ligne

This tool will allow you to use just for your CMS, many advanced features such as:

* Sending new videos while writing an article
* The ability to import more videos directly from your Drupal administration
* Sync between your VOD Infomaniak Network account and your Drupal CMS
* Automatic recovery of existing players
* Easy management and implementation of your playlist in our administration interface


INSTALLATION
------------
It is required to use this plugin to have an account on our VOD administration
interface http://statslive.infomaniak.ch/

If you would like more information about our VOD service, please go to the
address http://streaming.infomaniak.com/stockage-video-en-ligne

For updates, they are automatically proposed on your own administration
interface of Drupal.

FAQ
---
Q: I must provide my personal identifiers plugin?
A: This works, but for security reasons, it is strongly
advised to do so.
It is much more careful in your administration interface VOD,
create a new user and assign only the rights "Management API."
In case of problem, it will be much easier to remove the user or
change his password compromise all its services.
Q: I created a player, a folder or playlist, but they do not appear
not yet on my site
A: The plugin is intended to synchronize regularly with your account
to automatically retrieve the latest changes.
It may happen that you do not have time to wait for that
synchronizes automatically.
In this case, you must go on the Manage VOD > Configuration and
press the "Quick Sync" button.
Q: I sent videos, but they do not appear in the list of videos on
the site
A: This may indicate a problem with the address of callback. This is a
address used by our coding system to prevent your site that
new video is available.
This address must be reachable publicly. for more information,
refer to page VOD Management> Configuration
Q: I have not found the answer to my question
A: On our website, we have a Frequently Asked Question
(http://hosting.infomaniak.ch/support/faq/categories/toutes-157-1.html) which
answers many questions and provides guides / tutorials videos.
If you do not find an answer to your question, you can also us
by email (support-vod-drupal@infomaniak.ch)