<?php

/**
 * @file
 * EasyVod_db.inc
 *
 *
 * Table management module.
 *
 * @category Ajax
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

/**
 * VodInfomaniakEasyvodDb.
 *
 * Class for table management module.
 *
 * @category Ajax
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakEasyvodDb {

  /**
   * Returns all players.
   *
   * @return array
   *   Returns the result
   */
  public static function getPlayers() {
    return db_select('vod_player', 'p')->fields('p')->execute()->fetchAll();
  }


  /**
   * Returns all the information a player.
   *
   * @param int $player_code
   *   Player Code
   *
   * @return object
   *   Returns the result
   */
  public static function getPlayer($player_code) {
    return db_select('vod_player', 'p')->fields('p')->condition('p.iPlayer', intval($player_code), '=')->range(0, 1)->execute()->fetchObject();
  }


  /**
   * Removes players in the database.
   *
   * @return void
   *   Execute the action
   */
  public static function cleanPlayers() {
    db_delete('vod_player')->execute();
  }


  /**
   * Adding a player in a database.
   *
   * @param int     $player_code
   *   Player Code
   * @param string  $name
   *   Player name
   * @param int     $width
   *   Player width
   * @param int     $height
   *   Player height
   * @param bool $auto_play
   *   flag to starts automatically
   * @param bool $loop
   *   flag to loop again
   * @param date    $edit_date
   *   Date of publishing
   * @param bool $switch_quality
   *   flag to automatically switch quality
   *
   * @return void
   *   Execute the action
   */
  public static function insertPlayer($player_code, $name, $width, $height, $auto_play, $loop, $edit_date, $switch_quality) {
    db_insert('vod_player')->fields(array(
        'iPlayer' => $player_code,
        'sName' => $name,
        'iWidth' => $width,
        'iHeight' => $height,
        'bAutoPlay' => $auto_play,
        'bLoop' => $loop,
        'dEdit' => $edit_date,
        'bSwitchQuality' => $switch_quality,
      ))->execute();
  }


  /**
   * Retourn the number of player.
   *
   * @return int
   *   Returns the number of player
   */
  public static function countPlayer() {
    $query = db_select('vod_player', 'p')->fields('p');
    $nbr = $query->countQuery()->execute()->fetchField();
    return $nbr;
  }


  /**
   * Returns the identifier of the default player.
   *
   * @return int
   *   Returns the identifier of the default player
   */
  public static function getDefaultPlayerCode() {
    return db_select('vod_player', 'p')->fields('p', array('iPlayer'))->range(0, 1)->execute()->fetchField();
  }


  /**
   * Returns a playlist.
   *
   * @param int $playlist_code
   *   Playlist code
   *
   * @return int
   *   Returns the result
   */
  public static function getPlaylist($playlist_code) {
    return db_select('vod_playlist', 'p')->fields('p')->condition('iPlaylistCode', $playlist_code, '=')->execute()->fetchObject();
  }

  /**
   * Get playlists.
   *
   * @return array
   *   Returns the playlists
   */
  public static function getPlaylists() {
    return db_select('vod_playlist', 'p')->fields('p')->execute()->fetchAll();
  }


  /**
   * Clean playlists.
   *
   * @return void
   *   Execute the action
   */
  public static function cleanPlaylists() {
    db_delete('vod_playlist')->execute();
  }


  /**
   * Add playlist.
   *
   * @param int     $playlist_code
   *   Playlist Code
   * @param string  $name
   *   Playlist name
   * @param string  $description
   *   Playlist description
   * @param int     $total
   *   Total associated video
   * @param string $mode
   *   Playlist mode
   * @param date    $created_date
   *   Date of creation
   * @param int $total_duration
   *   Total duration
   *
   * @return void
   *   Execute the action
   */
  public static function insertPlaylist($playlist_code, $name, $description, $total, $mode, $created_date, $total_duration) {
    db_insert('vod_playlist')->fields(array(
        'iPlaylistCode' => $playlist_code,
        'sPlaylistName' => $name,
        'sPlaylistDescription' => $description,
        'iTotal' => $total,
        'sMode' => $mode,
        'dCreated' => $created_date,
        'iTotalDuration' => $total_duration,
      ))->execute();
  }

  /**
   * Returns the number of playlist.
   *
   * @return int
   *   Returns the total playlist
   */
  public static function countPlaylists() {
    $query = db_select('vod_playlist', 'p')->fields('p');
    $nbr = $query->countQuery()->execute()->fetchField();
    return $nbr;
  }


  /**
   * Get directory.
   *
   * @param int $folder_code
   *   Directory code
   *
   * @return array
   *   Return directory
   */
  public static function getFolder($folder_code) {
    return db_select('vod_folder', 'f')->fields('f')->condition('iFolder', $folder_code, '=')->execute()->fetchObject();
  }

  /**
   * Returns the list of directories.
   *
   * @param string $filter
   *   Filter
   *
   * @return array
   *   Returns the list of directories
   */
  public static function getFolders($filter = "") {
    if (empty($filter) === FALSE) {
      return db_select('vod_folder', 'f')->fields('f')->condition('iFolder', $filter, '=')->orderBy('iFolder', 'ASC')->execute()->fetchAll();
    }
    else {
      return db_select('vod_folder', 'f')->fields('f')->orderBy('sPath', 'ASC')->execute()->fetchAll();
    }
  }


  /**
   * Cleans directories.
   *
   * @return void
   *   Execute the action
   */
  public static function cleanFolders() {
    db_delete('vod_folder')->execute();
  }


  /**
   * Adding a directory database.
   *
   * @param int     $folder_code
   *   Directory code
   * @param string  $path
   *   Directory path
   * @param string  $name
   *   Directory name
   * @param string  $access
   *   Type of access to the directory
   * @param string $token
   *   Token
   *
   * @return void
   *   Execute the action
   */
  public static function insertFolder($folder_code, $path, $name, $access, $token) {
    db_insert('vod_folder')->fields(array(
        'iFolder' => $folder_code,
        'sPath' => $path,
        'sName' => $name,
        'sAccess' => $access,
        'sToken' => $token,
      ))->execute();
  }

  /**
   * Returns the total number of directories.
   *
   * @return void
   *   Execute the action
   */
  public static function countFolder() {
    $query = db_select('vod_folder', 'f')->fields('f');
    $nbr = $query->countQuery()->execute()->fetchField();
    return $nbr;
  }


  /**
   * Returns the last video.
   *
   * @return object
   *   Returns the last video
   */
  public static function getLastVideo() {
    return db_select('vod_video', 'v')->fields('v')->orderBy('dUpload', 'DESC')->range(0, 1)->execute()->fetchAssoc();
  }


  /**
   * Returns a video.
   *
   * @param int $video_code
   *   Video code
   *
   * @return object
   *   Returns the video
   */
  public static function getVideo($video_code) {
    return db_select('vod_video', 'v')->fields('v')->condition('iVideo', $video_code, '=')->range(0, 1)->execute()->fetchObject();
  }


  /**
   * Returns a list of videos depending on the filter.
   *
   * @param string $filter
   *   Filter
   *
   * @return array
   *   Returns the list of videos
   */
  public static function getVideos($filter = "") {
    if (empty($filter) === FALSE) {
      $folders = db_select('vod_folder', 'f')->fields('f')->condition('iFolder', $filter, '=')->orderBy('iFolder', 'ASC')->execute()->fetchAll();
      $folders_list = array();
      foreach ($folders as $folder) {
        $folders_list[] = $folder->iFolder;
      }

      $test = implode(",", $folders_list);
      $query = db_select('vod_video', 'v');
      $query->join('vod_folder', 'f', 'v.iFolder = f.iFolder');
      $query->fields('v')->fields('f', array('sAccess', 'sToken'))->condition('v.iFolder', array($test), 'IN')->orderBy('v.dUpload', 'DESC');
      $max_count = $query->countQuery()->execute()->fetchField();
      $num_per_page = 20;
      $page = pager_default_initialize($max_count, $num_per_page);
      $offset = $num_per_page * $page;
      $query->range($offset, $num_per_page);
      $result = $query->execute()->fetchAll();
      return $result;
    }
    else {
      $query = db_select('vod_video', 'v');
      $query->join('vod_folder', 'f', 'v.iFolder = f.iFolder');
      $query->fields('v')->fields('f', array('sAccess', 'sToken'))->orderBy('v.dUpload', 'DESC');
      $max_count = $query->countQuery()->execute()->fetchField();
      $num_per_page = 20;
      $page = pager_default_initialize($max_count, $num_per_page);
      $offset = $num_per_page * $page;
      $query->range($offset, $num_per_page);
      $result = $query->execute()->fetchAll();
      return $result;
    }
  }


  /**
   * Search videos depending on the filter.
   *
   * @param string $search
   *   Keyword Search
   * @param string $filter
   *   Filter
   *
   * @return array
   *   Returns the list of videos
   */
  public static function getVideosWithSearch($search, $filter = "") {
    if (!empty($filter)) {
      $folders = db_select('vod_folder', 'f')->fields('f')->condition('iFolder', $filter, '=')->orderBy('iFolder', 'ASC')->execute()->fetchAll();
      $folders_list = array();
      foreach ($folders as $folder) {
        $folders_list[] = $folder->iFolder;
      }

      $test = implode(",", $folders_list);
      $query = db_select('vod_video', 'v');
      $query->join('vod_folder', 'f', 'v.iFolder = f.iFolder');
      $query->fields('v')->fields('f', array('sAccess', 'sToken'))->condition('v.iFolder', array($test), 'IN')->condition('v.sName', '%' . $search . '%', 'LIKE')->orderBy('v.dUpload', 'DESC');
      $result = $query->execute()->fetchAll();
      return $result;
    }
    else {
      $query = db_select('vod_video', 'v');
      $query->join('vod_folder', 'f', 'v.iFolder = f.iFolder');
      $query->fields('v')->fields('f', array('sAccess', 'sToken'))->condition('v.sName', '%' . $search . '%', 'LIKE')->orderBy('v.dUpload', 'DESC');
      $result = $query->execute()->fetchAll();
      return $result;
    }
  }


  /**
   * Returns videos by the server code.
   *
   * @param string $server_code
   *   Server code
   * @param int $folder_code
   *   Directory code
   *
   * @return array
   *   Returns the list of videos
   */
  public static function getVideosByCodes($server_code, $folder_code) {
    $query = db_select('vod_video', 'v');
    $query->fields('v')->condition('v.sServerCode', $server_code, '=')->condition('v.iFolder', intval($folder_code), '=');
    $result = $query->execute()->fetchAll();
    return $result;
  }


  /**
   * Cleans videos.
   *
   * @return void
   *   Exécute the action
   */
  public static function cleanVideos() {
    db_delete('vod_video')->execute();
  }


  /**
   * Renames a video.
   *
   * @param int $video_code
   *   Video code
   * @param string $name
   *   New name
   *
   * @return void
   *   Execute the action
   */
  public static function renameVideo($video_code, $name) {
    db_update('vod_video')->fields(array('sName' => $name))->condition('iVideo', $video_code, '=')->execute();
  }


  /**
   * Add video.
   *
   * @param int    $video_code
   *   Video code
   * @param int    $folder_code
   *   Directory code
   * @param string $name
   *   Video name
   * @param string $server_code
   *   Server code
   * @param string $path
   *   Video path
   * @param string $extension
   *   Extension Video
   * @param int    $duration
   *   Video Length
   * @param string $upload_date
   *   Date of uploading the video
   *
   * @return void
   *   Execute the action
   */
  public static function insertVideo($video_code, $folder_code, $name, $server_code, $path, $extension, $duration, $upload_date) {
    db_insert('vod_video')->fields(array(
        'iVideo' => $video_code,
        'iFolder' => $folder_code,
        'sName' => $name,
        'sPath' => $path,
        'sServerCode' => $server_code,
        'sExtension' => $extension,
        'iDuration' => $duration,
        'dUpload' => $upload_date,
      ))->execute();
  }


  /**
   * Returns the number of video in a directory.
   *
   * @param string $filter
   *   Filter
   *
   * @return array
   *   Returns the list of videos
   */
  public static function countVideo($filter = "") {
    if (empty($filter) === FALSE) {
      $folders = db_select('vod_folder', 'f')->fields('f', array('iFolder'))->condition('sPath', $filter, 'LIKE')->orderBy('sPath', 'ASC')->execute()->fetchAll();
      $folders_list = array();
      foreach ($folders as $folder) {
        $folders_list[] = $folder->iFolder;
      }

      $query = db_select('vod_video', 'v')->fields('v')->condition('iFolder', implode(',', $folders_list), 'IN');
      $nbr = $query->countQuery()->execute()->fetchField();
      return $nbr;
    }
    else {
      $query = db_select('vod_video', 'v')->fields('v');
      $nbr = $query->countQuery()->execute()->fetchField();
      return $nbr;
    }
  }


  /**
   * Deleting a video.
   *
   * @param int $video_code
   *   Video code
   *
   * @return void
   *   Execute the action
   */
  public static function deleteVideo($video_code = -1) {
    db_delete('vod_video')->condition('iVideo', $video_code, '=')->execute();
  }


  /**
   * Updating an upload.
   *
   * @param string $token
   *   Token
   * @param string $post_code
   *   Post code
   *
   * @return void
   *   Execute the action
   */
  public static function updateUpload($token, $post_code) {
    $condition = 'drupal_vod_upload' . $token;
    db_update('vod_upload')->fields(array('iVideo' => $post_code))->condition($condition, $token, '=')->execute();
  }


  /**
   * Returns a video uploader.
   *
   * @param string $token
   *   Token
   *
   * @return object
   *   Returns the video
   */
  public static function getUploadVideo($token) {
    $db = JFactory::getDBO();
    $query = "SELECT * FROM `jmla_vod_upload` WHERE sToken='" . $token . "' LIMIT 1";
    $db->setQuery($query);
    return $db->loadAssoc();
  }


  /**
   * Returns the state of the video uploader treatment.
   *
   * @return object
   *   Returns the result
   */
  public static function getUploadProcess() {
    return db_select('vod_upload', 'u')->fields('u')->condition('u.iVideo', 0, '=')->range(0, 1)->execute()->fetchAssoc();
  }


  /**
   * Returns an option.
   *
   * @param string $option
   *   Option name
   *
   * @return object
   *   Returns the result
   */
  public static function getOption($option) {
    return db_select('vod_option', 'o')->fields('o', array('sOptionValue'))->condition('o.sOptionName', $option, '=')->range(0, 1)->execute()->fetchField();
  }


  /**
   * Updating an option.
   *
   * @param string $option
   *   Option name
   * @param string $option_value
   *   Option value
   *
   * @return void
   *   Execute the action
   */
  public static function updateOption($option, $option_value) {
    db_update('vod_option')->fields(array(
        'sOptionName' => $option,
        'sOptionValue' => $option_value,
      ))->condition('sOptionName', $option, '=')->execute();
  }


  /**
   * Add option.
   *
   * @param string $option
   *   Option name
   * @param string $option_value
   *   Option value
   *
   * @return void
   *   Execute the action
   */
  public static function insertOption($option, $option_value) {
    db_insert('vod_option')->fields(array(
        'sOptionName' => $option,
        'sOptionValue' => $option_value,
      ))->execute();
  }
}
