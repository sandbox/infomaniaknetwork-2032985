<?php
/**
 * @file
 * File callback
 *
 * Various functions for the use of callback.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

/**
 * Callback.
 *
 * @param string   $key
 *   Key callback
 *
 * @return void
 *   Execute the function
 */
function vod_infomaniak_callback($key = '') {
  if ($key == '') {
    echo 'Probleme lors du callback';
    return FALSE;
  }
  else {
    $response = $_POST;
    if (VodInfomaniakEasyvodDb::getOption('vod_api_callbackKey') == $key) {
      if (empty($response['iFileCode']) === FALSE) {
        $video_code = intval($response['iFileCode']);
      }

      if (empty($response['iFolderCode']) === FALSE) {
        $folder_code = intval($response['iFolderCode']);
      }

      if (empty($response['sFileName']) === FALSE) {
        $filename = $response['sFileName'];
      }

      if (empty($response['sFileServerCode']) === FALSE) {
        $server_code = $response['sFileServerCode'];
      }

      if (empty($video_code) || empty($folder_code)) {
        echo 'Probleme avec les parametres';
        return FALSE;
      }

      $folder = VodInfomaniakEasyvodDb::getFolder($folder_code);
      if (empty($folder) || empty($folder->sName)) {
        echo 'Dossier inconnu';
        return FALSE;
      }

      $encodage    = array_shift($response['files']);
      $path_tmp    = explode('/redirect/' . VodInfomaniakEasyvodDb::getOption('vod_api_id') . '/', $encodage['sFileUrl']);
      $path        = '/' . dirname($path_tmp[1]) . '/';
      $extension   = strtoupper($encodage['sExtension']);
      $duration    = intval($response['iDuration']);
      $upload_date = date("Y-m-d H:i:s", strtotime($response['dDateUpload']));
      $old_video   = VodInfomaniakEasyvodDb::getVideosByCodes($server_code, $folder_code);

      if (empty($old_video) === FALSE) {
        foreach ($old_video as $video) {
          VodInfomaniakEasyvodDb::deleteVideo($video->iVideo);
        }
      }

      VodInfomaniakEasyvodDb::insertVideo($video_code, $folder_code, $filename, $server_code, $path, $extension, $duration, $upload_date);

      if (empty($response['sInfo']) === FALSE) {
        $param_info = $response['sInfo'];
        if (strpos($param_info, "wp_upload_post_") !== FALSE) {
          $token = str_replace("wp_upload_post_", "", $param_info);
          VodInfomaniakEasyvodDb::updateUpload($token, $video_code);
        }
      }
    }
    exit();
  }
}
