<?php
/**
 * @file
 * Search articles
 *
 * Search articles videos and displays the results in a table
 *
 * @category Ajax
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

chdir($_POST['drupalroot']);
define('DRUPAL_ROOT', $_POST['drupalroot']);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/includes/database/database.inc';
drupal_bootstrap(DRUPAL_ROOT);

require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';

/**
 * Test the existence of a file.
 *
 * @param string $url
 *   Url of the file to test
 *
 * @return bool
 *   Returns TRUE if the file exists
 */
function vod_infomaniak_exist_file($url) {
  $file   = @fopen($url, 'r');
  $return = $file;
  if ($file !== FALSE) {
    $return = TRUE;
    fclose($file);
  }

  return $return;

}


if (isset($_POST['folderFilter']) === TRUE) {
  $filter = $_POST['folderFilter'];
}
else {
  $filter = '';
}

$videos   = VodInfomaniakEasyvodDb::getVideosWithSearch($_POST['recherche'], $filter);
$compteur = 0;
$output   = '';

if (empty($videos) === FALSE) {
  $rows = '';
  foreach ($videos as $video) {
    $modulo = ($compteur % 2);
    if ($modulo === 0) {
      $class = 'odd';
    }
    else {
      $class = 'even';
    }

    $player_code = VodInfomaniakEasyvodDb::getOption('player');
    if (intval($player_code) === 0) {
      $player_code = VodInfomaniakEasyvodDb::getDefaultPlayerCode();
    }

    $rows .= '<tr style="border:none;cursor:pointer;" class="' . $class . '" onclick="addVideo(\'' . $_POST['vod_api_id'] . '\', \'' . $video->sPath . $video->sServerCode . '\', \'' . strtolower($video->sExtension) . '\', \'' . $player_code . '\', \'' . intval(VodInfomaniakEasyvodDb::getDefaultPlayerCode()) . '\', \'' . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . '\');">';
    $rows .= '<td style="border:none;cursor:pointer;"><label style="cursor:pointer;">' . $video->sName . '</label></td>';
    $rows .= '<td style="border:none;cursor:pointer;width:100px;"><label style="cursor:pointer;">' . date('d / m / Y ', strtotime($video->dUpload)) . '</label></td>';
    $rows .= '<td style="border:none;cursor:pointer;width:140px;text-align:center;">';

    if (vod_infomaniak_exist_file('http://vod.infomaniak.com/redirect/' . $_POST['vod_api_id'] . $video->sPath . $video->sServerCode . '.jpg') === TRUE) {
      $rows .= '<img style="width:130px;height:100px;border-radius:3px;" src="http://vod.infomaniak.com/redirect/' . $_POST['vod_api_id'] . $video->sPath . $video->sServerCode . '.jpg" alt="' . t("No picture to display for this video") . '" />';
    }

    $rows .= '</td></tr>';
    $compteur++;
  }

  $output .= '<div id="hiddenVideoFields" style="display:none;">';
  $output .= '<label>' . t('Search') . '<input onkeyup="searchVideo("' . DRUPAL_ROOT . '", "' . drupal_get_path('module', 'vod_infomaniak') . '", "' . VodInfomaniakEasyvodDb::getOption('vod_filter_folder') . '", "' . VodInfomaniakEasyvodDb::getOption('vod_api_id') . '");" type="text" id="recherche-input" name="input" value="" size="60" maxlength="128" class="form-text"></label>';
  $output .= '<div style="width:100%;overflow:auto;max-height:300px;border:1px solid #bebfb9;margin-top:10px;">';
  $output .= '<table style="border:none;" id="videolist">';
  $output .= '<tbody>' . $rows . '</tbody>';
  $output .= '</table></div></div>';
}
else {
  $output = '<label>&nbsp;' . t('No video available') . '</label>';
}

echo $output;
