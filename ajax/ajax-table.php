<?php
/**
 * @file
 * Table uploads
 *
 * Viewing recent uploads in a table via ajax
 *
 * @category Ajax
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

chdir($_POST['drupalroot']);
define('DRUPAL_ROOT', getcwd());
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
define('SALT', '8eq7f4qqa7dverta');

if (empty($_POST['drupalroot']) === FALSE) {
  include_once $_POST['drupalroot'] . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
}

$api        = vod_infomaniak_get_api();
$lastimport = $api->getLastImportation();
VodInfomaniakEasyvodDb::updateOption('vod_last_import', serialize($lastimport));
$html = vod_infomaniak_tab_last_upload($lastimport);
echo $html;


/**
 * View table of uploads.
 *
 * @param array $lastimport
 *   Table last uploads
 *
 * @return text
 *   Returns the html code containing the table.
 */
function vod_infomaniak_tab_last_upload($lastimport) {
  $return = '';
  if (empty($lastimport) === FALSE) {
    $return .= '<div id="tabImport">';
    $return .= '<span id="tabImportRefresh" style="float:left; padding-right: 20px; padding-top: 20px; padding-bottom: 20px;"></span>';
    $return .= '<br/><table class="adminlist" style="width: 99%""><thead><tr>';
    $return .= '<th> ' . t('File') . ' </th><th> ' . t('Date') . ' </th><th> ' . t('Status') . ' </th><th> ' . t('Description') . ' </th>';
    $return .= '</tr></thead><tbody>';
    foreach ($lastimport as $import) {
      $return .= '<tr>';
      $return .= '<td><label><img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/videofile.png" style="vertical-align:middle"/>' . $import['sFileName'] . '</label></td>';
      $return .= '<td>' . $import['dDateCreation'] . '</td>';
      $return .= '<td>';
      if ($import['sProcessState'] === 'OK') {
        $return .= '<label> <img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-tick.png" style="vertical-align:middle"/>' . t('OK') . '</label>';
      }
      elseif ($import['sProcessState'] === 'WARNING') {
        $return .= '<label> <img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/videofile.png" style="vertical-align:middle"/>' . t('OK (alerts were triggered)') . '</label>';
      }
      elseif ($import['sProcessState'] === 'DOWNLOAD') {
        $return .= '<label> <img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-download.png" style="vertical-align:middle"/>' . t('Downloading') . '</label>';
      }
      elseif ($import['sProcessState'] === 'WAITING' || $import['sProcessState'] === 'QUEUE' || $import['sProcessState'] === 'PROCESSING') {
        $return .= '<label> <img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ajax-loader.gif" style="vertical-align:middle"/>' . t('Converting video') . '</label>';
      }
      else {
        $return .= '<label> <img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-exclamation-yellow.png" style="vertical-align:middle"/>' . t('Errors') . '</label>';
      }

      $return .= '</td>';
      $return .= '<td width="50%">' . str_replace("'", '&#39', $import['sLog']) . '</td>';
      $return .= '</tr>';
    }

    $return .= '</tbody></table>';
    $return .= '</div>';
  }

  return $return;

}


/**
 * Recovery connection to the API.
 *
 * @return void
 *   Returns the connection
 */
function vod_infomaniak_get_api() {
  if (is_file($_POST['drupalroot'] . '/sites/all/modules/vod_infomaniak/vod.api.inc') === TRUE) {
    include_once $_POST['drupalroot'] . '/sites/all/modules/vod_infomaniak/vod.api.inc';
  }
  elseif (is_file($_POST['drupalroot'] . '/modules/vod_infomaniak/models/vod.api.inc') === TRUE) {
    include_once $_POST['drupalroot'] . '/modules/vod_infomaniak/models/vod.api.inc';
  }

  $password = vod_infomaniak_decrypt($_POST['password']);
  return new VodInfomaniakApi($_POST['login'], $password, $_POST['id']);

}


/**
 * Decrypts text.
 *
 * @param string $text
 *   Chain to go for decryption
 *
 * @return string
 *   Returns the decrypted text
 */
function vod_infomaniak_decrypt($text) {
  return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

}
