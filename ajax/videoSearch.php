<?php
/**
 * @file
 * Video search
 *
 * Video search and displays the results in a table
 *
 * @category Ajax
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

chdir($_POST['drupalroot']);
define('DRUPAL_ROOT', getcwd());

require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (empty($_POST['drupalroot']) === FALSE) {
  include_once $_POST['drupalroot'] . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
}

if (isset($_POST['folderFilter']) === TRUE) {
  $filter = $_POST['folderFilter'];
}
else {
  $filter = '';
}

$videos = VodInfomaniakEasyvodDb::getVideosWithSearch($_POST['recherche'], $filter);

if (empty($videos) === FALSE) {
  $headers = array(
    t('Video'),
    t('Folder'),
    t("Date uploaded"),
    t('Action'),
  );
  $rows    = array();

  foreach ($videos as $video) {
    $row1 = '';
    if ($video->sExtension === 'M4A' || $video->sExtension === 'MP3') {
      $row1 .= '<img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/audiofile.png" style="vertical-align:bottom"/>';
    }
    else {
      $row1 .= '<img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/videofile.png" style="vertical-align:bottom"/>';
    }

    $open_vod = "openVodPopup('" . VodInfomaniakEasyvodDb::getOption('vod_api_id') . "', '" . VodInfomaniakEasyvodDb::getOption('vod_api_group') . "','" . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . "', '" . $video->iVideo . "', '" . addslashes($video->sName) . "','" . $video->sPath . $video->sServerCode . "', '" . strtolower($video->sExtension) . "', '" . strtolower($video->sAccess) . "', '" . $video->sToken . "', '" . $video->iFolder . "'); return false;";
    $row1  .= '<a href="javascript:;" onclick="' . $open_vod . '">' . ucfirst(stripslashes($video->sName)) . '</a>';

    $row4  = '<a href="javascript:; return false;" onclick="' . $open_vod . '"><img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-information.png" alt="' . t('Information about this video') . '"/></a>';
    $row4 .= '<a href="https://statslive.infomaniak.com/vod/videoDetail.php/g<' . VodInfomaniakEasyvodDb::getOption('vod_api_group') . 's7i' . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . '?iFileCode=' . $video->iVideo . ' target="_blank"><img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-video.png" alt="' . t('Manage this video') . '"/></a>';
    $row4 .= '<a href="https://statslive.infomaniak.com/vod/videoDetail.php/g' . VodInfomaniakEasyvodDb::getOption('vod_api_group') . 's7i' . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . '?iFileCode=' . $video->iVideo . '&tab=2" target="_blank"><img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-statistics.png" alt="' . t('Display statistics for this video') . '"/></a>';
    $row4 .= '<a href="javascript:;" onclick="confirmVodDelete(\'' . $video->iVideo . '\' ,\'' . addslashes($video->sName) . '\')"><img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-delete.png" alt="' . t('Delete this video') . '"/></a>';
    $rows[] = array(
      $row1,
      '<img src="' . $_POST['drupalbasepath'] . '/' . $_POST['drupalmodulepath'] . '/images/ico-folder-open-16x16.png" style="vertical-align:bottom"/>' . $video->sPath,
      $video->dUpload,
      $row4,
    );
  }

  $output = '<table><thead align="left"><tr>';

  foreach ($headers as $header) {
    $output .= '<th>' . $header . '</th>';
  }

  $output  .= '</tr></thead><tbody>';
  $compteur = 0;

  foreach ($rows as $ligne) {
    $modulo = ($compteur % 2);
    if ($modulo === 0) {
      $class = 'odd';
    }
    else {
      $class = 'even';
    }

    $output .= '<tr class="' . $class . '"><td>' . $ligne[0] . '</td><td>' . $ligne[1] . '</td><td>' . $ligne[2] . '</td><td>' . $ligne[3] . '</td></tr>';
    $compteur++;
  }

  $output .= '</tbody></table>';
}
else {
  $output = '<span>' . t('No video available') . '</span>';
}

echo $output;
