<?php
/**
 * @file
 * View video
 *
 * Class that manages the views of videos
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakVideoView
 *
 * Class that manages the views of videos.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakVideoView extends VodInfomaniakBasicView {

  /**
   * Methods to load the CSS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerVideoCSS() {
    vod_infomaniak_register_css('//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', 'external');
    vod_infomaniak_register_css(static::$module . '/css/vodinfomaniak.css', 'file');
  }


  /**
   * Methods to load the JS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerVideoJS() {
    vod_infomaniak_register_js(static::$module . '/js/editor_plugin.js', 'file');
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js', 'external');
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', 'external');
    vod_infomaniak_register_js('search = function(){
                  var sRecherche = jQuery(\'#edit-input\').val();
                  if(sRecherche == ""){
                  jQuery(".pager").show();
                  }else{
                  jQuery(".pager").hide();
                  }
                  jQuery.ajax({
                        type: "post",
                        url: "' . static::$base . '/' . static::$module . '/ajax/videoSearch.php",
                        data: {recherche:sRecherche,drupalroot:\'' . static::$sDrupalRoot . '\',folderFilter:\'' . static::$iFilterFolder . '\',drupalmodulepath:\'' . static::$module . '\',drupalbasepath:\'' . static::$base . '\'},
                        success: function(html){
                          jQuery("#edit-videotable2").html(html);
                          jQuery("#edit-videotable").hide();
                          jQuery("#edit-videotable2").show();
                        }
                    });
                }', 'inline');
  }


  /**
   * Form display videos.
   *
   * @param array   $videos
   *   List of videos
   *
   * @return void
   *   Returns the html page
   */
  public static function displayVideoForm($videos) {
    $form = array();
    $pager = theme('pager');

    if (empty($videos) === FALSE) {
      $headers = array(
        t('Video'),
        t('Folder'),
        t('Date uploaded'),
        t('Action'),
      );

      $rows = array();
      foreach ($videos as $video) {
        $row1 = '';

        if ($video->sExtension == "M4A" || $video->sExtension == "MP3") {
          $row1 .= '<img src="' . static::$base . '/' . static::$module . '/images/audiofile.png" style="vertical-align:bottom"/>';
        }
        else {
          $row1 .= '<img src="' . static::$base . '/' . static::$module . '/images/videofile.png" style="vertical-align:bottom"/>';
        }

        $open_vod = "openVodPopup('" . static::$sApiId . "', '" . static::$iGroupe . "', '" . static::$iService . "', '" . $video->iVideo . "', '" . addslashes($video->sName) . "', '" . $video->sPath . $video->sServerCode . "', '" . strtolower($video->sExtension) . "', '" . strtolower($video->sAccess) . "', '" . $video->sToken . "', '" . $video->iFolder . "'); return false;";
        $row1 .= ' <a href="javascript:;" onclick="' . $open_vod . '">' . ucfirst(stripslashes($video->sName)) . '</a>';

        $row2 = '<a href="javascript:; return false;" onclick="' . $open_vod . '"><img src="' . static::$base . '/' . static::$module . '/images/ico-information.png" alt="' . t('Information about this video') . '"/></a>';
        $row2 .= '<a href="https://statslive.infomaniak.com/vod/videoDetail.php/g<' . static::$iGroupe . 's7i' . static::$iService . '?iFileCode=' . $video->iVideo . ' target="_blank"><img src="' . static::$base . '/' . static::$module . '/images/ico-video.png" alt="' . t('Manage this video') . '"/></a> ';
        $row2 .= '<a href="https://statslive.infomaniak.com/vod/videoDetail.php/g' . static::$iGroupe . 's7i' . static::$iService . '?iFileCode=' . $video->iVideo . '&tab=3" target="_blank"><img src="' . static::$base . '/' . static::$module . '/images/ico-statistics.png" alt="' . t('Display statistics for this video') . '"/></a> ';
        $row2 .= '<a href="javascript:;" onclick="confirmVodDelete(\'' . $video->iVideo . '\' ,\'' . addslashes($video->sName) . '\')"><img src="' . static::$base . '/' . static::$module . '/images/ico-delete.png" alt="' . t('Delete this video') . '"/></a>';
        $rows[] = array(
          $row1,
          '<img src="' . static::$base . '/' . static::$module . '/images/ico-folder-open-16x16.png" style="vertical-align:bottom"/> ' . $video->sPath,
          $video->dUpload,
          $row2,
        );
      }

      $form['vod_infomaniak_videos']['input'] = array(
        '#type' => 'textfield',
        '#prefix' => '<div id="customSearch" style="text-align: left;"><img src="' . static::$base . '/' . static::$module . '/images/search.png" style="display:inline-block;vertical-align:middle;">',
        '#suffix' => '</div>' . $pager,
        '#attributes' => array(
          'onkeyup' => 'search();',
          'size' => '30',
          'placeholder' => t('Search') . '...',
        ),
      );

      $form["vod_infomaniak_videos"]["table"] = array(
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
        '#attributes' => array('id' => 'edit-videotable'),
      );

      $form["vod_infomaniak_videos"]["table2"] = array(
        '#markup' => '<div id="edit-videotable2"></div>',
        '#attributes' => array('id' => 'edit-videotable2', 'display' => 'none'),
      );

      $form['vod_infomaniak_videos']['dialog-confirm-id'] = array(
        '#type' => 'hidden',
        '#attributes' => array('id' => 'dialog-confirm-id'),
        '#default_value' => '',
      );

      $form['vod_infomaniak_videos']['dialog-confirm-title'] = array(
        '#type' => 'hidden',
        '#attributes' => array('id' => "dialog-confirm-title"),
        '#default_value' => "",
      );

      $form['vod_infomaniak_videos']["submit-delete"] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('vod_infomaniak_videos_form_submit_delete'),
        '#attributes' => array('style' => 'display:none;'),
      );

      $form['vod_infomaniak_videos']["submit-post"] = array(
        '#type' => 'submit',
        '#value' => t('Post'),
        '#submit' => array('vod_infomaniak_videos_form_submit_post'),
        '#attributes' => array('style' => 'display:none;'),
      );

      $form['vod_infomaniak_videos']["pagination"] = array('#markup' => $pager);

      $form["vod_infomaniak_videos"]["popup"] = array(
        '#type' => 'fieldset',
        '#title' => t('Video preview'),
        '#prefix' => '<div id="dialog-modal-vod" style="display:none; padding: 5px; overflow: hidden;">',
        '#suffix' => '</div>',
        '#attributes' => array('style' => 'border:none;'),
      );

      $form["vod_infomaniak_videos"]["popup"]["informations"] = array(
        '#type' => 'fieldset',
        '#title' => t('Information'),
      );

      $form["vod_infomaniak_videos"]["popup"]["informations"]["iframe"] = array(
        '#markup' => '<iframe id="dialog-modal-video" frameborder="0" width="480" height="320" src="#"></iframe>',
      );

      $form['vod_infomaniak_videos']["popup"]["informations"]['dialog-modal-name'] = array(
        '#type' => 'textfield',
        '#prefix' => '<div id="custom-top-panel">',
        '#title' => t('Name'),
        '#default_value' => '',
        '#attributes' => array('style' => 'width: 393px; border 1px solid #CCC; border-radius: 3px; background-color: #FFF; padding: 4px; border: 1px solid #CCCCCC; color: #444444;'),
      );

      $form['vod_infomaniak_videos']["popup"]["informations"]["vod_infomaniak_submit_videos"] = array(
        '#type' => 'submit',
        '#value' => t('Rename'),
        '#submit' => array('vod_infomaniak_videos_form_submit_rename'),
      );

      $content  = '<br/><div id="dialog-modal-access-block" style="padding-top: 2px;display:inline-block;">';
      $content .= '<label style="display:inline-block;">' . t("Access restriction") . ' :</label>';
      $content .= '<span id="dialog-modal-access" style="padding-left: 45px;display:inline-block;"></span>';
      $content .= '</div></div>';
      $form["vod_infomaniak_videos"]["popup"]["informations"]['dialog-modal-access'] = array('#markup' => $content);

      $form['vod_infomaniak_videos']["popup"]["informations"]['dialog-modal-id'] = array(
        '#type' => 'hidden',
        '#attributes' => array('id' => 'dialog-modal-id'),
        '#default_value' => '',
      );

      $form["vod_infomaniak_videos"]["popup"]["integration"] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="custom-bottom-panel">',
        '#suffix' => '</div>',
        '#title' => t('Integration'),
      );

      $style_input = 'width: 393px; border 1px solid #CCC; border-radius: 3px; background-color: #FFF; padding: 4px; border: 1px solid #CCCCCC; color: #444444;';
      $style_link = 'text-decoration: none; color:#444444; font-weight: bold;';
      $content  = '<p>';
      $content .= '<label>' . t('Video URL') . ':</label>';
      $content .= '<input id="dialog-modal-url" text="" style="' . $style_input . '" readonly="value" onfocus="this.select();"/>';
      $content .= '<a id="dialog-modal-url-href" href="#" target="_blank">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-redo.png" style="margin-right:25px; vertical-align:middle;"  alt="' . t('Display video') . '"/>';
      $content .= '</a>';
      $content .= '</p>';
      $content  = '<p>';

      $content .= '<label>' . t("Image URL") . ':</label>';
      $content .= '<input id="dialog-modal-url-img" text="" style="' . $style_input . '" readonly="value" onfocus="this.select();"/>';
      $content .= '<a id="dialog-modal-url-img-href" href="#" target="_blank">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-redo.png" style="margin-right:25px; vertical-align:middle;"  alt="' . t('Preview image') . '"/>';
      $content .= '</a>';
      $content .= '</p>';
      $content .= '</div>';
      $content .= '<div style="margin-left:100px;">';
      $content .= '<ul>';
      $content .= '<li>';

      $url_return = static::$base . '/' . static::$module . '/ajax/videoSearch.php';
      $return_action = "closeVodPopup('" . $url_return . "', '" . static::$sDrupalRoot . "', '" . static::$iFilterFolder . "', '" . static::$module . "', '" . static::$base . "')";

      $content .= '<a id="dialog-modal-admin4" href="javascript:;" onclick="' . $return_action . '" style="' . $style_link . '">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-redo.png" alt="' . t('Return') . '" style="vertical-align:bottom"/> ' . t('Return');
      $content .= '</a>';
      $content .= '</li>';
      $content .= '<li style="padding-top: 10px">';
      $content .= '<a id="dialog-modal-admin2" href="#" target="_blank" style="' . $style_link . '">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-statistics.png" alt="' . t('Display statistics for this video') . '" style="vertical-align:bottom"/> ' . t('Display statistics');
      $content .= '</a>';
      $content .= '</li>';
      $content .= '<li style="padding-top: 10px">';
      $content .= '<a id="dialog-modal-admin3" href="javascript:;" onclick="jQuery(\'#edit-submit-post\').click();" style="' . $style_link . '">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-edit.png" alt="' . t('Write an article') . '" style="vertical-align:bottom"/> ' . t('Write an article');
      $content .= '</a>';
      $content .= '</li>';
      $content .= '<li style="padding-top: 10px">';
      $content .= '<a id="dialog-modal-admin" href="#" target="_blank" style="' . $style_link . '">';
      $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-video.png" alt="' . t('Manage this video') . '" style="vertical-align:bottom"/> ' . t('Manage this video');
      $content .= '</a>';
      $content .= '</li>';
      $content .= '</ul>';
      $content .= '</div>';
      $form["vod_infomaniak_videos"]["popup"]["integration"]['blockintegration'] = array('#markup' => $content);
    }
    else {
      $form["vod_infomaniak_videos"]["table"] = array('#markup' => '<label>' . t('No video available') . '</label>');
    }
    return static::setFormHtml($form);

  }

}
