<?php
/**
 * @file
 * View player
 *
 * Class that manages the views of the player
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakPlayerView
 *
 * Class that manages the views of the player.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakPlayerView extends VodInfomaniakBasicView {

  /**
   * Methods to load the CSS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerPlayerCSS() {
    vod_infomaniak_register_css('//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', 'external');
  }


  /**
   * Methods to load the JS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerPlayerJS() {
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js', 'external');
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', 'external');
    vod_infomaniak_register_js('jQuery(document).ready(function () {
        PlayerInfo = function(){
          jQuery(\'.player-info\').hide();
          value = jQuery(\'#edit-selectplayer\').val();
          $("#edit-variable").val(value);
          jQuery(\'#player-info-\'+value).show();
          jQuery("#player-demo-video").attr( "src", "http://vod.infomaniak.com/iframe.php?url=infomaniak_11_vod/demo-2362/mp4-226/big_buck_bunny_720p_h264.mp4&player="+value+"&vod=' . static::$iService . '&preloadImage=infomaniak_11_vod/demo-2362/mp4-226/big_buck_bunny_720p_h264.jpg" );
        }
        PlayerInfo();
      });', 'inline');
  }

  /**
   * Player display in the Administration interface.
   *
   * @param array   $players
   *   List of players
   * @param array   $options
   *   Options
   * @param string   $selected
   *   Default
   *
   * @return void
   *   Returns the html page
   */
  public static function displayPlayerForm($players, $options, $selected) {
    $form = array();
    $form['gestionvideos_player'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default video integration'),
      '#description' => t('Selection of player by default') . ':',
    );

    $form['gestionvideos_player']['selectplayer'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $selected,
      '#attributes' => array('onchange' => "PlayerInfo()"),
    );

    $form['gestionvideos_player']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Choose this player'),
    );

    $form['gestionvideos_player']['infos'] = array(
      '#type' => 'fieldset',
      '#title' => t('Information about this player') . ':',
    );

    if (empty($players) === TRUE) {
      $form['gestionvideos_player']['infos']['playerframe'] = array('#markup' => '<label>' . t('No player available') . '</label>');
    }
    else {
      foreach ($players as $player) {
        $autoload = 'Oui';
        if ($player->bAutoPlay == 0) {
          $autoload = 'Non';
        }

        $loop = 'Oui';
        if ($player->bLoop == 0) {
          $loop = 'Non';
        }

        $switch = 'Oui';
        if ($player->bSwitchQuality == 0) {
          $switch = 'Non';
        }

        $playerdate = date("d M Y H:i", strtotime($player->dEdit));
        $url        = 'https://statslive.infomaniak.com/vod/players/playerConfig.php/g' . static::$iGroupe . 's7i' . static::$iService . '?iPlayerCode=' . $player->iPlayer;
        $content    = '<div id="player-info-' . $player->iPlayer . '" class="player-info" style="float:left; padding: 5px 15px; margin: 0px 5px; display:none; width: 45%;">';
        $content   .= '<ul>';
        $content   .= '<li><b>' . t('Name') . ':</b> ' . ucfirst($player->sName) . '</li>';
        $content   .= '<li><b>' . t('Date') . ':</b> ' . $playerdate . '</li>';
        $content   .= '<li><b>' . t('Resolution') . ':</b> ' . $player->iWidth . 'x' . $player->iHeight . '</li>';
        $content   .= '<li><b>' . t('Playback starts automatically') . ':</b> ' . $autoload . '</li>';
        $content   .= '<li><b>' . t('Loop playback') . ':</b> ' . $loop . '</li>';
        $content   .= '<li><b>' . t('Quality switch') . ':</b> ' . $switch . '</li>';
        $content   .= '</ul>';
        $content   .= '<div style="text-align:center; width: 100%;border-top:1px solid #eee;padding-top:20px;margin-top:20px;">';
        $content   .= '<a id="dialog-modal-admin" href="' . $url . '" target="_blank" style="text-decoration: none; color:#444444; font-weight:bold;font-size:1.150em;"><img src="' . static::$base . '/' . static::$module . '/images/ico-edit.png" alt="' . t('Edit this player') . '" style="vertical-align:top"/> ' . t('Edit this player') . '</a>';
        $content   .= '</div>';
        $content   .= '</div>';

        $form['gestionvideos_player']['infos']['playerframe_' . $player->iPlayer] = array('#markup' => $content);
      }
    }

    $form['gestionvideos_player']['infos']['showplayer'] = array(
      '#markup' => '<iframe id="player-demo-video" style="float:left;" frameborder="0" width="480" height="360" src="#"></iframe>',
    );

    $form['gestionvideos_createplayer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Creating or editing players'),
      '#description' => t('To edit or create new flash players, please log into your VOD control panel') . ": <a href='https://statslive.infomaniak.com/vod/player.php/g" . static::$iGroupe . "s7i" . static::$iService . "' target='_blank'>" . t('Access player configuration') . "</a>",
    );

    $content  = t('You may customize your video player so that it integrates fully with your web site. Here are a few of the advanced features available') . ':';
    $content .= '<ul style="margin-left: 15px; list-style: disc inside;">';
    $content .= '<li>' . t('Seek function allowing you to move to a given position in a long video almost instantly.') . '</li>';
    $content .= '<li>' . t('A button allowing you to switch instantly between different video qualities.') . '</li>';
    $content .= '<li>' . t('Export button to increase the visibility of your video on different social networks (Facebook, Twitter).') . '</li>';
    $content .= '<li>' . t('Exportable player allowing visitors to retrieve code to embed the video in another page.') . '</li>';
    $content .= '<li>' . t('Easy player customization via quick edits (toolbar colors, size, logo) or the option to use of an fully customized toolbar.') . '</li>';
    $content .= '<li>' . t('Adswizz compatible allowing you to easily add advertising before or after video playback.') . '</li>';
    $content .= '<li>' . t('With more options waiting to be discovered...') . '</li>';
    $content .= '</ul>';

    $form['gestionvideos_moreoptions'] = array(
      '#type' => 'fieldset',
      '#title' => t("Plus d'options"),
      '#description' => $content,
    );
    return static::setFormHtml($form);
  }
}
