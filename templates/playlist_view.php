<?php
/**
 * @file
 * View the playlist
 *
 * Class that manages the views of the playlist
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakPlaylistView
 *
 * Class that manages the views of the playlist.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakPlaylistView extends VodInfomaniakBasicView {

  /**
   * Methods to load the JS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerPlaylistJS() {
    vod_infomaniak_register_js('
     postPlaylist = function(code){
      jQuery("#playlistcode").val(code);
      jQuery("#edit-postplaylist").click();
     }', 'inline');
  }


  /**
   * Display form a playlist.
   *
   * @param array   $playlist
   *   Playlist
   *
   * @return void
   *   Returns the html page
   */
  public static function displayPlaylistForm($playlist) {
    $form = array();
    $headers = array(
      t('Name'),
      t('Description'),
      t('Number of videos'),
      t('Duration'),
      t('Play mode'),
      t('Date'),
      t('Actions'),
    );
    $rows = array();

    if (empty($playlist) === FALSE) {
      foreach ($playlist as $playlist) {
        if ($playlist->iTotalDuration) {
          $duration = intval(intval($playlist->iTotalDuration) / 100);
        }
        $hour = intval($duration / 3600);
        $min = intval($duration / 60) % 60;
        $sec = intval($duration) % 60;

        $time = '';
        if ($hour > 0) {
          $time .= $hour . 'h. ';
        }

        if (empty($time) === FALSE || $min > 0) {
          $time .= $min . 'm. ';
        }

        if (empty($time) === FALSE || $sec > 0) {
          $time .= $sec . 's. ';
        }

        $duree = '&nbsp;';
        if (empty($time) === FALSE) {
          $duree = $time;
        }

        $desc = '&nbsp;';
        if (empty($playlist->sPlaylistDescription) === FALSE) {
          $desc = ucfirst($playlist->sPlaylistDescription);
        }

        $rows[] = array(
          '<img src="' . static::$base . '/' . static::$module . '/images/ico-display-list.png" style="vertical-align:bottom; padding: 0px 5px;"/>' . ucfirst($playlist->sPlaylistName),
          $desc,
          $playlist->iTotal,
          $duree,
          $playlist->sMode,
          $playlist->dCreated,
          '<a href="https://statslive.infomaniak.com/vod/playlists.php/g' . static::$iGroupe . 's7i' . static::$iService . '?sAction=showPlaylist&iPlaylistCode=' . $playlist->iPlaylistCode . '" target="_blank"><img src="' . static::$base . '/' . static::$module . '/images/ico-information.png" title ="' . t('Manage this playlist') . '" alt="' . t('Manage this playlist') . '"/></a> <a href="javascript:;" onclick="postPlaylist(' . $playlist->iPlaylistCode . ')"><img src="' . static::$base . '/' . static::$module . '/images/ico-edit.png" title ="' . t('Write an article') . '" alt="' . t('Write an article') . '"/></a>',
        );
      }

      $form["gestionvideos_array"] = array(
        '#type' => 'fieldset',
        '#title' => t('Playlists'),
        '#description' => t('If you wish to add or change the playlists below, please go to') . ' <a href="https://statslive.infomaniak.com/vod/playlists.php/g' . static::$iGroupe . 's7i' . static::$iService . '" target="_blank">' . t('the control panel') . '</a>',
      );

      $form["gestionvideos_array"]["table"] = array(
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
      );

      $form['gestionvideos_playlists']['playlistcode'] = array(
        '#type' => 'hidden',
        '#attributes' => array('id' => "playlistcode"),
        '#default_value' => '',
      );

      $form['gestionvideos_playlists']["postplaylist"] = array(
        '#type' => 'submit',
        '#value' => t('Post'),
        '#submit' => array('vod_infomaniak_playlists_form_submit_post'),
        '#attributes' => array('style' => 'display:none;'),
      );
    }
    else {
      $form["gestionvideos_array"]["table"] = array('#markup' => '<label>' . t('No playlist available') . '</label>');
    }
    return static::setFormHtml($form);

  }
}
