<?php
/**
 * @file
 * For configuration
 *
 * Class that manages the view of the configuration page
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakConfigurationView
 *
 * Class that manages the view of the configuration page.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakConfigurationView extends VodInfomaniakBasicView {

  /**
   * Display the form authentication.
   *
   * @param string $login
   *   Login
   * @param string $password
   *   Password
   * @param string $vod_id
   *   VOD ID
   * @param bool $connected
   *   Defines whether you are connected
   *
   * @return text
   *   Returns the html code containing the form.
   */
  public static function displayAuthForm($login, $password, $vod_id, $connected) {
    $form['vod_infomaniak_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('VOD plugin administration'),
      '#description' => t('To work properly, the plugin needs to interface with your Infomaniak VOD account.') . '<br/>' . t('For security reasons it is strongly recommended that you created a new dedicated user in your Infomaniak control panel with permissions restricted to API access only.') . '<br/>' . t('For more information please go to the "Settings -> Api & Callback" section of your VOD control panel.'),
    );

    $form['vod_infomaniak_config']['vod_infomaniak_login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#default_value' => $login,
      '#required' => TRUE,
    );

    $form['vod_infomaniak_config']['vod_infomaniak_password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#default_value' => $password,
      '#required' => TRUE,
    );

    $form['vod_infomaniak_config']['vod_infomaniak_idvod'] = array(
      '#type' => 'textfield',
      '#title' => t("VOD Space identifier"),
      '#default_value' => $vod_id,
      '#required' => TRUE,
    );

    if (empty($connected) === FALSE && $connected != 'off') {
      $connexion = "<span style='color: green;'>" . t('Connect') . "</span>";
    }
    else {
      if ($login != '') {
        $connexion = "<span style='color: red;'>" . t('Cannot connect') . "</span>";
      }
      else {
        $connexion = "<span style='color: red;'>" . t('No connected') . "</span>";
      }
    }

    $form['vod_infomaniak_config']['vod_infomaniak_connected'] = array(
      '#title' => t('Connexion'),
      '#markup' => '<label>' . t('Connecting:') . ' ' . $connexion . '</label><br/>',
    );

    $form['vod_infomaniak_config']['vod_infomaniak_submit1'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#submit' => array('vod_infomaniak_configuration_form_submit1'),
    );
    return static::setFormHtml($form);

  }

  /**
   * Display form the configuration page.
   *
   * @param int $count_video
   *   Total videos
   * @param int $count_folder
   *   Total directories
   * @param int $count_player
   *   Total players
   * @param int $count_playlist
   *   Total playlists
   * @param array $options
   *   Options
   * @param string $selected
   *   Default
   *
   * @return text
   *   Returns the html code containing the form.
   */
  public static function displayConnectedForm($count_video, $count_folder, $count_player, $count_playlist, $options, $selected) {
    $form['vod_infomaniak_synchro'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Synchronization'),
      '#description'   => t('To work correctly, this extension needs to synchronize with your VOD account on a regular basis.') . '<br/>' . t('This allows you to keep an up to date list of players, folders and playlists on your blog.') . '<br/>' . t('This operation automatically takes place on a fairly regular basis, but you can also force a check below.'),
    );

    $form['vod_infomaniak_synchro']['compteurs'] = array(
      '#markup'        => '<p><label>' . t("Videos retrieved") . ': ' . $count_video . '</label></span></p><p><label>' . t('Folders retrieved') . ': ' . $count_folder . '</label></span></p><p><label>' . t('Players retrieved') . ': ' . $count_player . '</label></span></p><p><label>' . t('Playlists retrieved') . ': ' . $count_playlist . '</label></span></p>',
    );

    $form['vod_infomaniak_synchro']['vod_infomaniak_submit_fast'] = array(
      '#type'          => 'submit',
      '#value'         => t('Quick sync'),
      '#submit'        => array('vod_infomaniak_configuration_form_submit_fast'),
    );

    $form['vod_infomaniak_synchro']['vod_infomaniak_submit_full'] = array(
      '#type'          => 'submit',
      '#value'         => t('Data synchronization'),
      '#submit'        => array('vod_infomaniak_configuration_form_submit_full'),
    );

    $form['vod_infomaniak_folder'] = array(
      '#type'          => 'fieldset',
      '#title'         => t("Filtering access to your VOD space"),
      '#description'   => t("By default, this plugin allows access to all videos/folders in your VOD space.") . '<br/>' . t("Under some circumstances, it may be useful to limit site access to just a subsection of users.") . '<br/>' . t('The option below allows you to restrict access to this site to a particular folder and all subfolders.'),
    );

    $form['vod_infomaniak_folder']['vod_infomaniak_selectfolder'] = array(
      '#type'          => 'select',
      '#options'       => $options,
      '#default_value' => $selected,
    );

    $form['vod_infomaniak_folder']['vod_infomaniak_submit_folder'] = array(
      '#type'          => 'submit',
      '#value'         => t('Submit'),
      '#submit'        => array('vod_infomaniak_configuration_form_submit_folder'),
    );

    $form['vod_infomaniak_callback'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Callback Configuration'),
      '#description'   => t('This option will allow you to automatically update your blog every time a video is added to your VOD space.'),
    );

    $form['vod_infomaniak_callback']['description'] = array(
      '#markup' => t('Please go to') . ' <a href="https://statslive.infomaniak.com/vod/configuration.php/g' . static::$iGroupe . 's7i' . static::$iService . '" target="_blank">Settings -> Api & Callback</a> ' . t('and enter the following URL in the "Callback Address"') . '<br/><p><label style="font-weight: bold;">' . t('Callback address') . ':</label><label><span>' . static::$base . '/admin/config/media/vod_infomaniak/callback/' . static::$sCallbackKey . '</span></label></p>',
    );

    return static::setFormHtml($form);
  }
}
