<?php
/**
 * @file
 * For the upload
 *
 * Class that manages the views of the upload
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakUploadView
 *
 * Class that manages the views of the upload.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakUploadView extends VodInfomaniakBasicView {

  /**
   * Display base form.
   *
   * @param array   $options
   *   Folder Options
   * @param string   $tab
   *   Markup
   *
   * @return void
   *   Returns the html page
   */
  public static function displayUploadBaseForm($options, $tab) {
    $form = array();
    $form['vod_infomaniak_upload'] = array(
      '#type' => 'fieldset',
      '#title' => t('Uploading a new video'),
      '#description' => t('This plugin allows you to add new videos directly from this blog. To do so, simply choose a folder then follow the instructions') . ':',
    );

    $form["vod_infomaniak_upload"]["vod_infomaniak_upload_labeldossier"] = array('#markup' => '<br/><label><b>1.</b> ' . t('Select upload folder') . ' :</label>');

    $form['vod_infomaniak_upload']['vod_infomaniak_upload_selectfolder'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#attributes' => array('onchange' => 'changeFolder()', 'onkeyup' => 'changeFolder()'),
    );

    $form["vod_infomaniak_upload"]["vod_infomaniak_upload_labelmethode"] = array(
      '#prefix' => '<div id="submitLine" style="display:none;" class="submit">',
      '#markup' => '<label><b>2.</b> ' . t('Select upload type') . ' :</label><br/>',
    );

    $form['vod_infomaniak_upload']["vod_infomaniak_upload_submitupload"] = array(
      '#type' => 'submit',
      '#value' => t('Upload from this computer'),
      '#submit' => array('vod_infomaniak_upload_form_submit_upload'),
    );

    $form['vod_infomaniak_upload']["vod_infomaniak_upload_submitimport"] = array(
      '#type' => 'submit',
      '#suffix' => '</div>',
      '#value' => t('Import from another site'),
      '#submit' => array('vod_infomaniak_upload_form_submit_import'),
    );

    $form["vod_infomaniak_upload"]["vod_infomaniak_upload_labeltable"] = array(
      '#markup' => '<label style="font-weight:bold;">' . t('Previous Uploads') . '</label>',
    );

    $form["vod_infomaniak_upload"]["vod_infomaniak_upload_table"] = array(
      '#prefix' => '<div id="tabImport">',
      '#suffix' => '</div>',
      '#markup' => $tab,
    );
    return static::setFormHtml($form);

  }


  /**
   * Viewing the upload form.
   *
   * @param string   $folder
   *   Directory name
   * @param string   $folder_path
   *   Directory path
   *
   * @return void
   *   Returns the html page
   */
  public static function displayUploadUploadForm($folder, $folder_path) {
    $form = array();
    if (empty($folder_path) === TRUE) {
      $folder_path = '/';
    }

    $content  = '<p style="margin-top: 0px;">';
    $content .= '<a href="javascript:;" onclick="location.reload();" style="text-decoration: none; color:#444444;">';
    $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-redo.png" alt="' . t('Return') . '" style="vertical-align:bottom"/> ' . t('Return');
    $content .= '</a></p>';

    $form['vod_infomaniak_upload_return'] = array('#markup' => $content);
    $form['vod_infomaniak_upload_fromcomputer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Video upload utility'),
      "vod_infomaniak_upload_description" => array(),
    );

    $content  = '</p><p>';
    $content .= '<label style="font-weight: bold">' . t('Upload folder') . ':';
    $content .= '<span><img src="' . static::$base . '/' . static::$module . '/images/ico-folder-open-16x16.png" style="vertical-align:bottom"/>  ' . $folder . ' ( ' . $folder_path . ' )</span></label>';
    $content .= '</p><p>';
    $content .= '<label style="font-weight: bold">' . t('Limits') . ':</label>';
    $content .= '<ul style="list-style: disc inside; margin-left: 20px;">';
    $content .= '<li>' . t('The maximum file size for uploads is 1GB') . '</li>';
    $content .= '<li>' . t('Supported video formats are') . ' avi, flv, mov, mpeg, mp4, mkv, rm, wmv, m4v, vob, 3gp, webm, f4v, ts</li>';
    $content .= '<li>' . t('The upload must be completed within 4 hours') . '</li>';
    $content .= '</ul></p>';
    $content .= '<p><label style="font-weight: bold">' . t('Uploading') . ':</label></p>';
    $content .= '<div id="up"></div>';

    $form["vod_infomaniak_upload_fromcomputer"]["vod_infomaniak_upload_description"] = array('#markup' => $content);
    return static::setFormHtml($form);

  }

  /**
   * Viewing the import form.
   *
   * @param string   $folder
   *   Directory name
   * @param string   $folder_path
   *   Directory path
   *
   * @return void
   *   Returns the html page
   */
  public static function displayUploadImportForm($folder, $folder_path) {
    $form = array();
    if (empty($folder_path) === TRUE) {
      $folder_path = '/';
    }

    $content  = '<p style="margin-top: 0px;">';
    $content .= '<a href="javascript:;" onclick="location.reload();" style="text-decoration: none; color:#444444;">';
    $content .= '<img src="' . static::$base . '/' . static::$module . '/images/ico-redo.png" alt="' . t('Return') . '" style="vertical-align:bottom"/> ' . t('Return');
    $content .= '</a></p>';

    $form['vod_infomaniak_upload_return'] = array('#markup' => $content);

    $form['vod_infomaniak_upload_fromurl'] = array(
      '#type' => 'fieldset',
      '#title' => t('Video upload utility'),
    );

    $content  = '<p>';
    $content .= '<label style="font-weight: bold">' . t('Upload folder') . ':';
    $content .= '<span><img src="' . static::$base . '/' . static::$module . '/images/ico-folder-open-16x16.png" style="vertical-align:bottom"/>  ' . $folder . ' ( ' . $folder_path . ' )</span></label>';
    $content .= '</p>';
    $content .= '<p>';
    $content .= '<label style="font-weight: bold">' . t('Limits') . ':</label>';
    $content .= '<ul style="list-style: disc inside; margin-left: 20px;">';
    $content .= '<li>' . t('The maximum file size for uploads is 1GB') . '</li>';
    $content .= '<li>' . t('Supported video formats are') . ' avi, flv, mov, mpeg, mp4, mkv, rm, wmv, m4v, vob, 3gp, webm, f4v, ts</li>';
    $content .= '</ul>';
    $content .= '</p>';

    $form["vod_infomaniak_upload_fromurl"]["vod_infomaniak_upload_fromurl_description"] = array('#markup' => $content);

    $form['vod_infomaniak_upload_fromurl']['vod_infomaniak_upload_fromurl_selectprotocol'] = array(
      '#type' => 'select',
      '#title' => t('Header') . ':',
      '#options' => array(
        "http" => t('http://'),
        "https" => t('https://'),
        "ftp" => t('ftp://'),
      ),
    );

    $form['vod_infomaniak_upload_fromurl']['vod_infomaniak_upload_fromurl_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address') . ':',
      '#required' => TRUE,
      '#attributes' => array(
        'style' => 'width:50%;',
        'onkeyup' => 'checkURL();',
        'showsuccess' => 'false',
      ),
    );

    $form['vod_infomaniak_upload_fromurl']["vod_infomaniak_upload_fromurl_checkauth"] = array(
      '#type' => 'checkbox',
      '#title' => t('This address requires authentication.'),
      '#attributes' => array('onclick' => 'checkAuth();'),
    );

    $form['vod_infomaniak_upload_fromurl']['vod_infomaniak_upload_fromurl_authfieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Authentification'),
      '#prefix' => '<div id="authLine" style="display:none">',
      '#suffix' => '</div>',
    );

    $form['vod_infomaniak_upload_fromurl']['vod_infomaniak_upload_fromurl_authfieldset']['vod_infomaniak_upload_fromurl_login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login') . ':',
    );

    $form['vod_infomaniak_upload_fromurl']['vod_infomaniak_upload_fromurl_authfieldset']['vod_infomaniak_upload_fromurl_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password') . ':',
    );

    $form['vod_infomaniak_upload_fromurl']["vod_infomaniak_upload_fromurl_submit"] = array(
      '#type' => 'submit',
      '#value' => t('Import'),
      '#submit' => array('vod_infomaniak_upload_form_submit_fromurl'),
    );
    return static::setFormHtml($form);

  }


  /**
   * Methods to load the CSS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadCSS() {
    vod_infomaniak_register_css('//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', 'external');
    vod_infomaniak_register_css(static::$module . '/css/vodinfomaniak.css', 'file');
  }


  /**
   * Method to load the JS.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadJS() {
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js', 'external');
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', 'external');
    vod_infomaniak_register_js('//ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js', 'external');
    vod_infomaniak_register_js('//vod.infomaniak.com/apiUpload/flashUpload.js', 'external');
    vod_infomaniak_register_js('
        var changeFolder = function() {
            jQuery("#edit-vod-infomaniak-upload-fromcomputer").hide();
            jQuery("#edit-vod-infomaniak-upload-fromurl").hide();
            if (jQuery("#edit-vod-infomaniak-upload-selectfolder").val() != "") {
                jQuery("#submitLine").show();
            } else {
                jQuery("#submitLine").hide();
            }
        };

        iAjaxRefresh = 5;
        iAjaxDecompte = 30;

        if( jQuery("#tabImportRefresh") && jQuery("#tabImport") ){
            setTimeout(function(){update_vod_import();}, 30000);
            setTimeout(function(){update_info();}, 100);
        }

        var update_info = function() {
            if (iAjaxDecompte >= 0 ) {
                iAjaxDecompte -= 1;
                jQuery("#tabImportRefresh").html("<span style=\'font-style:italic;color: #666666;\'>' . t('Update in') . ' "+ (iAjaxDecompte*1+1) +" ' . t('seconds') . '</span>");
            }
            setTimeout(function(){update_info();}, 1000);
        };

        var update_vod_import = function() {
            iAjaxDecompte = 0;
            jQuery.ajax({
                type: "post",
                url: "' . DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/ajax/ajax-table.php",
                data:
                {
                    drupalroot:      "' . static::$sDrupalRoot . '",
                    login:           "' . static::$sApiLogin . '",
                    password:        "' . static::$sApiPassword . '",
                    id:              "' . static::$sApiId . '",
                    drupalmodulepath:"' . static::$module . '",
                    drupalbasepath:  "' . static::$base . '"
                },
                success: function(html){
                    jQuery("#tabImport").html(html);
                }
            });

            if (iAjaxRefresh < 10) {
                iAjaxDecompte = 30;
                setTimeout(function(){update_vod_import();}, 30000);
            } else if (iAjaxRefresh < 25 ) {
                iAjaxDecompte = 60;
                setTimeout(function(){update_vod_import();}, 60000);
            } else if (iAjaxRefresh < 35) {
                iAjaxDecompte = 120;
                setTimeout(function(){update_vod_import();}, 120000);
            } else if (iAjaxRefresh < 45) {
                iAjaxDecompte = 300;
                setTimeout(function(){update_vod_import();}, 300000);
            } else if (iAjaxRefresh < 60) {
                iAjaxDecompte = 600;
                setTimeout(function(){update_vod_import();}, 600000);
            } else {
                iAjaxDecompte = -1;
            }
            iAjaxRefresh++;
        };

        var uploadFinish = function() {
            iAjaxRefresh = 0;
            update_vod_import();
        };
    ', 'inline');
  }


  /**
   * JS for upload.
   *
   * @param string   $token
   *   Token
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadJS2($token) {
    vod_infomaniak_register_js('
            flashUpload(\'' . $token . '\');

            multiUploadCallback = function(json){
                oJson = eval(\'(\'+json+\')\');
                switch (oJson.sStatus){
                    case "init":
                        document.getElementById(\'up\').callbackInitialisation();
                        break;
                    case "complete":
                        document.getElementById(\'up\').callbackProcessing(oJson.iCurrent,true);
                        setTimeout(\'CallParentWindowFunction();\', 2500);
                        break;
                    case "error":
                        alert(\'upload error : \'+oJson.sOriginalFileName);
                        location.reload();
                        break;
                }
            }

            CallParentWindowFunction = function(){
                location.reload();
                uploadFinish();
                return false;
            }', 'inline');
  }


  /**
   * JS for upload V3.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadJS3() {
    vod_infomaniak_register_js('
            checkURL = function(){
                var url = jQuery(\'#edit-vod_infomaniak-upload-fromurl-address\').val();
                if (url.indexOf("http://") !=-1) {
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-selectprotocol\').val(\'http\');
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-address\').val( url.replace(/http:\/\//i, "") );
                }else if (url.indexOf("https://") !=-1) {
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-selectprotocol\').val(\'https\');
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-address\').val( url.replace(/https:\/\//i, "") );
                }else if (url.indexOf("ftp://") !=-1) {
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-selectprotocol\').val(\'ftp\');
                    jQuery(\'#edit-vod_infomaniak-upload-fromurl-address\').val( url.replace(/ftp:\/\//i, "") );
                }
            };

            checkAuth = function(){
                if( jQuery("#edit-vod_infomaniak-upload-fromurl-checkauth").is(\':checked\') ){
                    jQuery("#authLine").show();
                }else{
                    jQuery("#authLine").hide();
                }
            };
            checkAuth();

            CallParentWindowFunction = function(){
                location.reload();
                return false;
            };', 'inline');
  }


  /**
   * JS for upload V4.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadJS4() {
    vod_infomaniak_register_js('CallParentWindowFunction();', 'inline');
  }


  /**
   * JS for upload V5.
   *
   * @return void
   *   Returns the html page
   */
  public static function registerUploadJS5() {
    vod_infomaniak_register_js('jQuery(document).ready(function() { jQuery("#edit-vod-infomaniak-upload").hide(); });', 'inline');
  }

}
