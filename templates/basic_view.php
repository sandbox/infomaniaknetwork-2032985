<?php
/**
 * @file
 * Default view
 *
 * Class for managing the view
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

/**
 * VodInfomaniakBasicView
 *
 * Class for managing the view.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakBasicView {

  public static $aFormHtml;
  public static $iGroupe;
  public static $iService;
  public static $iFilterFolder;
  public static $sDrupalRoot;
  public static $sApiLogin;
  public static $sApiPassword;
  public static $sApiId;
  public static $base;
  public static $module;
  public static $sCallbackKey;


  /**
   * initialization.
   *
   * @param array $options
   *   Options
   *
   * @return void
   *   Execute the action
   */
  public static function init($options = array()) {
    if (empty($options) === FALSE) {
      foreach ($options as $key => $value) {
        static::$$key = $value;
      }
    }
    global $base_url;
    static::$base = $base_url;
  }


  /**
   * Display the configuration page.
   *
   * @return void
   *   Returns the html page
   */
  public static function displayConfigError() {
    global $base_url;
    $form = array();
    $form['vod_infomaniak_player'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Configuration'),
      '#description' => t('Please go to') . ' <a href="' . $base_url . '?q=admin/config/media/vod_infomaniak/configure">' . t('Configuration') . '</a> ' . t('to set up your account') . '.',
    );
    return static::setFormHtml($form);
  }


  /**
   * Attribute in the form html.
   *
   * @param array $data
   *   Data
   *
   * @return void
   *   Execute the action
   */
  public static function setFormHtml($data = array()) {
    if (empty(static::$aFormHtml) === TRUE) {
      static::$aFormHtml = array();
    }

    static::$aFormHtml = array_merge(static::$aFormHtml, $data);
    return static::$aFormHtml;
  }


  /**
   * Get the form html.
   *
   * @return void
   *   Execute the action
   */
  public static function getFormHtml() {
    return static::$aFormHtml;
  }
}
