<?php
/**
 * @file
 * For help
 *
 * Class that manages the view using
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

require_once 'basic_view.php';

/**
 * VodInfomaniakHelpView
 *
 * Class for managing view the help page.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakHelpView extends VodInfomaniakBasicView {

  /**
   * Display the help page.
   *
   * @return void
   *   Returns the html page
   */
  public static function displayHelpForm() {
    $form = array();
    $form['vod_infomaniak_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Video Guides Infomaniak'),
    );

    $form['vod_infomaniak_help']['vod_infomaniak_guidevideo'] = array(
      '#markup' => '<div id="guidevideo" align="center"><video controls src="http://vod.infomaniak.com/redirect/guides_infomania_vod/folder-1448/mp4-282/vod-new.mp4" poster="http://vod.infomaniak.com/redirect/guides_infomania_vod/folder-1448/mp4-282/vod-new.jpg" preload="auto" style="border:1px solid #ccc;">' . t('Video guide') . '</video><div>',
    );

    return static ::setFormHtml($form);
  }
}
