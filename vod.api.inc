<?php
/**
 * @file
 * Communication with the API
 *
 * Class for using the functions of the API vod.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

/**
 * VodInfomaniakApi
 *
 * Class for using the functions of the API vod.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakApi {

  protected $sLogin = "";
  protected $sPassword = "";
  protected $sId = "";
  protected $soap;


  /**
   * Builder with initialization login.
   *
   * @param string $login
   *   Login
   * @param string $password
   *   Password
   * @param string $identifiant_vod
   *   Identifying the VOD space
   *
   * @return void
   *   Returns nothing
   */
  public function __construct($login, $password, $identifiant_vod = "") {
    $this->sLogin = $login;
    $this->sPassword = $password;
    $this->sId = $identifiant_vod;
  }


  /**
   * Debug method.
   *
   * @param string $function
   *   Function used
   * @param string $exception
   *   Var_dump
   *
   * @return void
   *   Show error
   */
  protected function debug($function, $exception) {
    echo "<h4 style='color:red'>Debug :: VodInfomaniakApi -> " . $function . "()</h4><code>";
    var_dump($exception);
    echo "</code>";
  }


  /**
   * Method of testing the connection.
   *
   * @return bool
   *   True or False
   */
  public function ping() {
    $soap = $this->getSoapAdmin();
    if (empty($soap) === FALSE) {
      return $soap->ping();
    }
    return FALSE;

  }


  /**
   * Display method of execution time.
   *
   * @return bool
   *   True or False
   */
  public function time() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->time();
      }
    }
    catch (Exception $exception) {
      $this->debug("time", $exception);
    }

    return 0;

  }


  /**
   * Get VOD ID.
   *
   * @return int
   *   VOD ID
   */
  public function getServiceItemID() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return intval($soap->getServiceItemID());
      }
    }
    catch (Exception $exception) {
      $this->debug("getServiceItemID", $exception);
    }

    return 0;

  }


  /**
   * Get Group code.
   *
   * @return int
   *   Group ID
   */
  public function getGroupID() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return intval($soap->getGroupeID());
      }
    }
    catch (Exception $exception) {
      $this->debug("getGroupID", $exception);
    }

    return 0;

  }


  /**
   * Get number of video.
   *
   * @return int
   *   Nombre de vidéo
   */
  public function countVideo() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return intval($soap->countVideo());
      }
    }
    catch (Exception $exception) {
      $this->debug("countVideo", $exception);
    }

    return FALSE;

  }


  /**
   * Delete video.
   *
   * @param int $folder_code
   *   Directory code
   * @param string $folder_server
   *   Directory server
   *
   * @return bool
   *   True or False
   */
  public function deleteVideo($folder_code, $folder_server) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->deleteVideo($folder_code, $folder_server);
      }
    }
    catch (Exception $exception) {
      $this->debug("deleteVideo", $exception);
    }

    return FALSE;

  }


  /**
   * Rename video.
   *
   * @param int $folder_code
   *   Directory code
   * @param string $folder_server
   *   Directory server
   * @param string $video_name
   *   New name for the video
   *
   * @return bool
   *   True or False
   */
  public function renameVideo($folder_code, $folder_server, $video_name) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->setVideoTitle($folder_code, $folder_server, $video_name);
      }
    }
    catch (Exception $exception) {
      $this->debug("renameVideo", $exception);
    }

    return FALSE;

  }


  /**
   * Get last videos.
   *
   * @param int $limit
   *   Max video
   * @param string $page
   *   Get page
   *
   * @return array
   *   List of videos
   */
  public function getLastVideo($limit, $page) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getLastVideo($limit, $page);
      }
    }
    catch (Exception $exception) {
      $this->debug("getLastVideo", $exception);
    }

    return FALSE;

  }


  /**
   * Get last imports.
   *
   * @return array
   *   List of imports
   */
  public function getLastImportation() {
    $soap = $this->getSoapAdmin();
    try {
      if (empty($soap) === FALSE) {
        return $soap->getLastImportation(15);
      }
    }
    catch (Exception $exception) {
      $this->debug("getLastImportation", $exception);
    }

    return FALSE;

  }


  /**
   * Get directories.
   *
   * @return array
   *   List of directories
   */
  public function getFolders() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getFolders();
      }
    }
    catch (Exception $exception) {
      $this->debug("getFolders", $exception);
    }

    return FALSE;

  }


  /**
   * Method info if there was a mod on issues.
   *
   * @param date $date
   *   Start date for the control
   *
   * @return bool
   *   True or False
   */
  public function folderModifiedSince($date) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->folderModifiedSince($date);
      }
    }
    catch (Exception $exception) {
      $this->debug("folderModifiedSince", $exception);
    }

    return FALSE;

  }


  /**
   * Get players.
   *
   * @return array
   *   List of players
   */
  public function getPlayers() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getPlayers();
      }
    }
    catch (Exception $exception) {
      $this->debug("getPlayers", $exception);
    }

    return FALSE;

  }


  /**
   * Method info if there has been an on players modif.
   *
   * @param date $date
   *   Start date for the control
   *
   * @return bool
   *   True or False
   */
  public function playerModifiedSince($date) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->playerModifiedSince($date);
      }
    }
    catch (Exception $exception) {
      $this->debug("PlayerModifiedSince", $exception);
    }

    return FALSE;

  }


  /**
   * Get playlists.
   *
   * @return array
   *   List of playlists
   */
  public function getPlaylists() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getPlaylists();
      }
    }
    catch (Exception $exception) {
      $this->debug("getPlaylists", $exception);
    }

    return FALSE;

  }


  /**
   * Method info if there has been an on playlists modif.
   *
   * @param date $date
   *   Start date for the control
   *
   * @return bool
   *   True or False
   */
  public function playlistModifiedSince($date) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->playlistModifiedSince($date);
      }
    }
    catch (Exception $exception) {
      $this->debug("playlistModifiedSince", $exception);
    }

    return FALSE;

  }


  /**
   * Method for obtaining a token upload.
   *
   * @param string $path
   *   Path of the upload directory
   *
   * @return string
   *   Return token
   */
  public function initUpload($path) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->initUpload($path);
      }
    }
    catch (Exception $exception) {
      $this->debug("initUpload", $exception);
    }

    return FALSE;

  }


  /**
   * Launch method of downloading a video.
   *
   * @param string $path
   *   Path of the upload directory
   * @param string $url
   *   Url Video
   * @param array $options
   *   Additional options
   *
   * @return bool
   *   True or False
   */
  public function importFromUrl($path, $url, $options) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->importFromUrl($path, $url, $options);
      }
    }
    catch (Exception $exception) {
      $this->debug("importFromUrl", $exception);
    }

    return FALSE;

  }


  /**
   * Way of adding information to the video.
   *
   * @param string $token
   *   Token
   * @param string $info
   *   Information
   *
   * @return bool
   *   True or False
   */
  public function addInfo($token, $info) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->addInfo($token, $info);
      }
    }
    catch (Exception $exception) {
      $this->debug("addInfo", $exception);
    }

    return FALSE;

  }


  /**
   * Method that returns the callback url.
   *
   * @return string
   *   Callback url
   */
  public function getCallback() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getCallbackUrl();
      }
    }
    catch (Exception $exception) {
      $this->debug("getCallback", $exception);
    }

    return FALSE;

  }


  /**
   * Method that returns the callback url 2.
   *
   * @return array
   *   Callback url
   */
  public function getCallbackV2() {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->getCallbackUrlV2();
      }
    }
    catch (Exception $exception) {
      $this->debug("getCallback", $exception);
    }

    return FALSE;

  }


  /**
   * Method to set the callback url.
   *
   * @param string $url
   *   New callback url
   *
   * @return bool
   *   True or False
   */
  public function setCallback($url) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->setCallbackUrl($url);
      }
    }
    catch (Exception $exception) {
      $this->debug("setCallback", $exception);
    }

    return FALSE;

  }


  /**
   * Method to set the callback url V2.
   *
   * @param string $url
   *   New callback url
   *
   * @return bool
   *   True or False
   */
  public function setCallbackV2($url) {
    try {
      $soap = $this->getSoapAdmin();
      if (empty($soap) === FALSE) {
        return $soap->setCallbackUrlV2($url);
      }
    }
    catch (Exception $exception) {
      $this->debug("setCallback", $exception);
    }

    return FALSE;

  }


  /**
   * Recovery method soap admin.
   *
   * @return void
   *   Execute the function
   */
  protected function getSoapAdmin() {
    if (empty($this->oSoap) === FALSE) {
      return $this->oSoap;
    }
    else {
      $options = array(
        'trace' => 1,
        'encoding' => 'UTF-8',
      );

      $this->oSoap = new SoapClient('https://statslive-api.infomaniak.com/vod/vod_soap.wsdl', $options);

      try {
        $this->oSoap->__setSoapHeaders(array(
          new SoapHeader(
            'urn:vod_soap',
            'AuthenticationHeader',
            new VodInfomaniakSoapVodAuthentificationHeader($this->sLogin, $this->sPassword, $this->sId)
          ),
        ));
        return $this->oSoap;
      }
      catch (Exception $exception) {
        $this->debug("getSoapAdmin", $exception);
      }
      return FALSE;

    }

  }
}


ini_set("soap.wsdl_cache_enabled", 0);

/**
 * VodInfomaniakSoapVodAuthentificationHeader
 *
 * Class for the header authentication soap.
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */
class VodInfomaniakSoapVodAuthentificationHeader {
  public $Password;
  public $sLogin;
  public $sVod;

  /**
   * Builder with initialization login.
   *
   * @param string $login
   *   Login
   * @param string $password
   *   Password
   * @param string $identifiant_vod
   *   Identifying the VOD space
   *
   * @return void
   *   Returns nothing
   */
  public function __construct($login, $password, $identifiant_vod) {
    $this->sPassword = $password;
    $this->sLogin = $login;
    $this->sVod = $identifiant_vod;
  }

}
