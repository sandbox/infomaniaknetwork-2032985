<?php
/**
 * @file
 * Class admin
 *
 * Class for managing admin VOD
 *
 * @category View
 * @package  VOD_Infomaniak
 * @license  http://www.gnu.org/licenses/agpl.html GNU Affero General Public License
 * @link     http://www.infomaniak.com
 */

define("SALT", "8eq7f4qqa7dverta");


/**
 * Display the configuration page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration() {
  return drupal_get_form('vod_infomaniak_configuration_form');
}


/**
 * Display configuration form.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration_form() {
  global $base_url;

  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  vod_infomaniak_use_template('configuration');

  $login    = VodInfomaniakEasyvodDb::getOption('vod_api_login');
  $password = VodInfomaniakEasyvodDb::getOption('vod_api_password');
  if (empty($password) === FALSE) {
    $password = 'XXXXXX';
  }

  $vod_id       = VodInfomaniakEasyvodDb::getOption('vod_api_id');
  $is_connected = VodInfomaniakEasyvodDb::getOption('vod_api_connected');
  VodInfomaniakConfigurationView::displayAuthForm($login, $password, $vod_id, $is_connected);

  if (vod_infomaniak_plugin_ready() === TRUE) {
    vod_infomaniak_fast_synchro();
    VodInfomaniakConfigurationView::init(array(
      'iGroupe'      => VodInfomaniakEasyvodDb::getOption('vod_api_group'),
      'iService'     => VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice'),
      'base'         => $base_url,
      'module'       => drupal_get_path('module', 'vod_infomaniak'),
      'sCallbackKey' => VodInfomaniakEasyvodDb::getOption("vod_api_callbackKey"),
    ));

    $video_count    = intval(VodInfomaniakEasyvodDb::getOption('vod_countVideo'));
    $folder_count   = intval(VodInfomaniakEasyvodDb::getOption('vod_countFolder'));
    $player_count   = intval(VodInfomaniakEasyvodDb::getOption('vod_countPlayer'));
    $playlist_count = intval(VodInfomaniakEasyvodDb::getOption('vod_count_playlist'));
    $folders        = VodInfomaniakEasyvodDb::getFolders();
    $folder_filter  = VodInfomaniakEasyvodDb::getOption('vod_filter_folder');
    $options        = array();

    if (empty($folder_filter) === FALSE) {
      VodInfomaniakEasyvodDb::updateOption('vod_filter_folder', $folder_filter);
      $folder_filter = VodInfomaniakEasyvodDb::getOption('vod_filter_folder');
    }
    else {
      $folder_filter = '';
    }

    if (empty($folders) === TRUE) {
      $options[] = t('No folder available');
      $selected = 0;
    }
    else {
      $options[-1] = '--' . t('Root Folder') . '--';
      $selected = -1;

      foreach ($folders as $folder) {
        if (empty($folder_filter) === FALSE && $folder_filter == $folder->iFolder) {
          $options[$folder->iFolder] = t('Folder') . ' : /' . $folder->sPath . ' | ' . t('Name') . ' : ' . $folder->sName;
          $selected = $folder->iFolder;
        }
        else {
          $options[$folder->iFolder] = t('Folder') . ' : /' . $folder->sPath . ' | ' . t('Name') . ' : ' . $folder->sName;
        }
      }
    }

    VodInfomaniakConfigurationView::displayConnectedForm($video_count, $folder_count, $player_count, $playlist_count, $options, $selected);
  }

  return VodInfomaniakConfigurationView::getFormHtml();

}


/**
 * Action in the form Submit1 configuration.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration_form_submit1($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  global $base_url;

  $result_bool      = FALSE;
  $api_callback_key = VodInfomaniakEasyvodDb::getOption('vod_api_callbackKey');
  $api_c            = VodInfomaniakEasyvodDb::getOption('vod_api_c');

  if (empty($api_callback_key) === TRUE) {
    $temp = sha1((time() * rand()));
    VodInfomaniakEasyvodDb::updateOption('vod_api_callbackKey', $temp);
  }

  if (empty($api_c) === TRUE) {
    $temp = substr(sha1((time() * rand())), 0, 20);
    VodInfomaniakEasyvodDb::updateOption('vod_api_c', $temp);
  }

  VodInfomaniakEasyvodDb::updateOption('vod_api_login', stripslashes(htmlspecialchars($form_state['input']['vod_infomaniak_login'])));
  if ($form_state['input']['vod_infomaniak_password'] != "XXXXXX") {
    VodInfomaniakEasyvodDb::updateOption('vod_api_password', vod_infomaniak_encrypt(stripslashes(htmlspecialchars($form_state['input']['vod_infomaniak_password']))));
  }

  VodInfomaniakEasyvodDb::updateOption('vod_api_id', stripslashes(htmlspecialchars($form_state['input']['vod_infomaniak_idvod'])));
  VodInfomaniakEasyvodDb::updateOption('vod_api_connected', 'off');

  try {
    $api         = vod_infomaniak_get_api();
    $result_bool = $api->ping();

    if ($result_bool === TRUE) {
      VodInfomaniakEasyvodDb::updateOption('vod_api_connected', 'on');
      VodInfomaniakEasyvodDb::updateOption('vod_api_icodeservice', $api->getServiceItemID());
      VodInfomaniakEasyvodDb::updateOption('vod_api_group', $api->getGroupID());
      VodInfomaniakEasyvodDb::updateOption('vod_api_lastUpdate', 0);

      $api_callback_valid = VodInfomaniakEasyvodDb::getOption('vod_api_valid_callback');
      $api_callback_valid = VodInfomaniakEasyvodDb::getOption('vod_api_valid_callback');

      if (empty($api_callback_valid) === TRUE || $api_callback_valid == 'off') {
        $url = $api->getCallback();
        if (empty($url) === FALSE && strpos($url, str_replace('http://', '', $base_url)) !== FALSE) {
          $api->setCallback('');
        }

        $url2 = $api->getCallbackV2();
        $callback_v2 = TRUE;
        if ($url2 != FALSE) {
          foreach ($url2 as $callback) {
            if (strpos($callback['sUrl'], $base_url) !== FALSE) {
              $callback_v2 = FALSE;
            }
          }
        }

        if ($callback_v2 === TRUE) {
          $api->setCallbackV2($base_url . '/admin/config/media/vod_infomaniak/callback/' . VodInfomaniakEasyvodDb::getOption('vod_api_callbackKey'));
        }

        VodInfomaniakEasyvodDb::updateOption('vod_api_valid_callback', 'on');
      }

      if (VodInfomaniakEasyvodDb::countVideo() == 0) {
        $api                = vod_infomaniak_get_api();
        $max_video_per_page = 200;
        VodInfomaniakEasyvodDb::cleanVideos();
        $video_count = $api->countVideo();
        $page_total = floor(($video_count - 1) / $max_video_per_page);

        for ($current_page = 0; $current_page <= $page_total; $current_page++) {
          $videos = $api->getLastVideo($max_video_per_page, ($current_page * $max_video_per_page));
          foreach ($videos as $video) {
            VodInfomaniakEasyvodDb::insertVideo($video['iFileCode'], $video['iFolder'], $video['sFileName'], $video['sFileServerCode'], $video['aEncodes'][0]['sPath'], $video['aEncodes'][0]['eConteneur'], $video['fFileDuration'], $video['dFileUpload']);
          }
        }
      }
    }
  }
  catch (Exception $exception) {

  }

  $form_state['storage']['aFolders'] = VodInfomaniakEasyvodDb::getFolders();
  $form_state['rebuild'] = TRUE;

}


/**
 * Action on the configuration form submit fast.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration_form_submit_fast($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  VodInfomaniakEasyvodDb::updateOption('vod_api_lastUpdate', 0);
  vod_infomaniak_fast_synchro();
  $form_state['rebuild'] = TRUE;

}


/**
 * Action on the configuration form submit full.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration_form_submit_full($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  VodInfomaniakEasyvodDb::updateOption('vod_api_lastUpdate', 0);
  vod_infomaniak_full_synchro();
  $form_state['rebuild'] = TRUE;

}


/**
 * Action on the Form submit configuration folder.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_configuration_form_submit_folder($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';

  if ($form_state['input']['vod_infomaniak_selectfolder'] == -1) {
    VodInfomaniakEasyvodDb::updateOption('vod_filter_folder', '');
  }
  else {
    VodInfomaniakEasyvodDb::updateOption('vod_filter_folder', $form_state['input']['vod_infomaniak_selectfolder']);
  }

  $form_state['rebuild'] = TRUE;

}


/**
 * Returns the video page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_videos() {
  return drupal_get_form('vod_infomaniak_videos_form');

}


/**
 * Display Form Video page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_videos_form() {
  global $base_url;
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  vod_infomaniak_use_template('video');

  VodInfomaniakVideoView::init(array(
    'iGroupe'       => VodInfomaniakEasyvodDb::getOption('vod_api_group'),
    'iService'      => VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice'),
    'sDrupalRoot'   => DRUPAL_ROOT,
    'iFilterFolder' => VodInfomaniakEasyvodDb::getOption('vod_filter_folder'),
    'base'          => $base_url,
    'sApiId'        => VodInfomaniakEasyvodDb::getOption('vod_api_id'),
    'module'        => drupal_get_path('module', 'vod_infomaniak'),
  ));

  if (vod_infomaniak_plugin_ready() === FALSE) {
    VodInfomaniakVideoView::displayConfigError();
  }
  else {
    $api = vod_infomaniak_get_api();
    $server_videos = $api->getLastVideo(10, 0);

    $filter = VodInfomaniakEasyvodDb::getOption('vod_filter_folder');
    $videos = VodInfomaniakEasyvodDb::getVideos($filter);

    for ($i = 0; $i < count($videos); $i++) {
      if (empty($videos[$i]->sToken) === FALSE) {
        $videos[$i]->sToken = vod_infomaniak_get_temporary_key($videos[$i]->sToken, $videos[$i]->sServerCode);
      }
    }

    if (count($server_videos) > count($videos)) {
      VodInfomaniakEasyvodDb::cleanVideos();
      vod_infomaniak_full_synchro();
      $videos = VodInfomaniakEasyvodDb::getVideos($filter);
      for ($i = 0; $i < count($videos); $i++) {
        if (empty($videos[$i]->sToken) === FALSE) {
          $videos[$i]->sToken = vod_infomaniak_get_temporary_key($videos[$i]->sToken, $videos[$i]->sServerCode);
        }
      }
    }

    VodInfomaniakVideoView::registerVideoJS();
    VodInfomaniakVideoView::registerVideoCSS();
    VodInfomaniakVideoView::displayVideoForm($videos);
  }

  return VodInfomaniakVideoView::getFormHtml();

}


/**
 * Renaming a video.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_videos_form_submit_rename($form, &$form_state) {
  $video = VodInfomaniakEasyvodDb::getVideo(intval($form_state['input']['dialog-modal-id']));

  if ($video != FALSE) {
    $api = vod_infomaniak_get_api();
    $api->renameVideo($video->iFolder, $video->sServerCode, $form_state['input']['dialog-modal-name']);
    VodInfomaniakEasyvodDb::renameVideo(intval($form_state['input']['dialog-modal-id']), $form_state['input']['dialog-modal-name']);
  }

  $form_state['rebuild'] = TRUE;

}


/**
 * Deleting a video.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_videos_form_submit_delete($form, &$form_state) {
  $video = VodInfomaniakEasyvodDb::getVideo(intval($form_state['input']['dialog-confirm-id']));
  if ($video != FALSE) {
    $api = vod_infomaniak_get_api();
    $api->deleteVideo($video->iFolder, $video->sServerCode);
    VodInfomaniakEasyvodDb::deleteVideo(intval($form_state['input']['dialog-confirm-id']));
  }

}


/**
 * Editing a video.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_videos_form_submit_post($form, &$form_state) {
  global $user;
  $video = VodInfomaniakEasyvodDb::getVideo(intval($form_state['input']['dialog-modal-id']));

  if ($video != FALSE) {
    $player_code = VodInfomaniakEasyvodDb::getOption('player');
    if (intval($player_code) == 0) {
      $player_code = VodInfomaniakEasyvodDb::getDefaultPlayerCode();
    }

    $url     = VodInfomaniakEasyvodDb::getOption('vod_api_id') . $video->sPath . $video->sServerCode . '.' . $video->sExtension . '&preloadImage=' . VodInfomaniakEasyvodDb::getOption('vod_api_id') . $video->sPath . $video->sServerCode . '.jpg&player=' . $player_code . '&vod=' . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . '&autostart=0&loop=0';
    $content = '<div align="center" width="100%"><iframe src="http://vod.infomaniak.com/iframe.php?url=' . $url . '" frameborder="0" width="480" height="360"> </iframe></div>';

    $node           = new stdClass();
    $node->type     = "article";
    node_object_prepare($node);
    $node->title    = $video->sName;
    $node->language = LANGUAGE_NONE;
    $node->uid      = $user->uid;

    $node->body[$node->language][0]['value']   = $content;
    $node->body[$node->language][0]['summary'] = text_summary($content);
    $node->body[$node->language][0]['format']  = 'full_html';

    $path       = 'node_created_on' . date('YmdHis');
    $node->path = array('alias' => $path);
    node_save($node);
    $form_state['redirect']  = 'node/' . $node->nid;
  }

}


/**
 * Display Form upload.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_upload_form($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  global $base_url;

  vod_infomaniak_use_template('upload');

  if (vod_infomaniak_plugin_ready() === FALSE) {
    VodInfomaniakUploadView::displayConfigError();
  }
  else {
    VodInfomaniakUploadView::init(array(
      'iGroupe'      => VodInfomaniakEasyvodDb::getOption('vod_api_group'),
      'iService'     => VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice'),
      'sDrupalRoot'  => DRUPAL_ROOT,
      'sApiLogin'    => VodInfomaniakEasyvodDb::getOption('vod_api_login'),
      'sApiPassword' => VodInfomaniakEasyvodDb::getOption('vod_api_password'),
      'sApiId'       => VodInfomaniakEasyvodDb::getOption('vod_api_id'),
      'base'         => $base_url,
      'module'       => drupal_get_path('module', 'vod_infomaniak'),
    ));

    $folders = VodInfomaniakEasyvodDb::getFolders(VodInfomaniakEasyvodDb::getOption('vod_filter_folder'));
    VodInfomaniakUploadView::registerUploadJS();
    VodInfomaniakUploadView::registerUploadCSS();

    if (isset($form_state['storage']['method']) === FALSE) {
      $options = array();

      if (empty($folders) === TRUE) {
        $options[] = t('No folder available');
      }
      else {
        $options[''] = '--' . t('Upload folder') . '--';
      }

      foreach ($folders as $folder) {
        $options[$folder->iFolder] = t('Folder') . ': / ' . $folder->sPath . ' | ' . t('Nom') . ': ' . $folder->sName;
      }

      $tab = vod_infomaniak_get_last_import();
      VodInfomaniakUploadView::displayUploadBaseForm($options, $tab);
    }
    elseif (isset($form_state['storage']['method']) === TRUE && $form_state['storage']['method'] == 'upload') {
      VodInfomaniakUploadView::registerUploadJS5();
      if (isset($form_state['storage']['selected']) === TRUE) {
        $folder_code = $form_state['storage']['selected'];
      }
      else {
        $folder_code = 0;
      }

      $folder = VodInfomaniakEasyvodDb::getFolder($folder_code);

      if (empty($folder) === TRUE || empty($folder->sName) === TRUE) {
        die("Il n'est pas possible d'uploader dans ce dossier.");
      }

      $api = vod_infomaniak_get_api();
      $token = $api->initUpload($folder->sPath);
      VodInfomaniakUploadView::displayUploadUploadForm($folder->sName, $folder->sPath);
      VodInfomaniakUploadView::registerUploadJS2($token);
      VodInfomaniakEasyvodDb::updateOption('vod_last_import', '');
    }
    elseif (isset($form_state['storage']['method']) === TRUE && $form_state['storage']['method'] == 'import') {
      VodInfomaniakUploadView::registerUploadJS5();
      $result_bool = FALSE;
      if (isset($form_state['storage']['selected']) === TRUE) {
        $folder_code = $form_state['storage']['selected'];
      }
      else {
        $folder_code = 0;
      }

      $folder = VodInfomaniakEasyvodDb::getFolder($folder_code);

      if (empty($folder) === TRUE || empty($folder->sName) === TRUE) {
        drupal_set_message("Il n'est pas possible d'uploader dans ce dossier.");
        die();
      }

      VodInfomaniakUploadView::displayUploadImportForm($folder->sName, $folder->sPath);
      VodInfomaniakUploadView::registerUploadJS3();
    }

    if (isset($form_state['storage']['bResult']) === TRUE && empty($form_state['storage']['bResult']) === FALSE) {
      VodInfomaniakUploadView::registerUploadJS4();
    }
  }

  return VodInfomaniakUploadView::getFormHtml();

}


/**
 * Action on import - upload page.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_upload_form_submit_import($form, &$form_state) {
  $form_state['rebuild']             = TRUE;
  $form_state['storage']['method']   = 'import';
  $form_state['storage']['selected'] = $form_state['input']['vod_infomaniak_upload_selectfolder'];

}


/**
 * Action on upload - upload page.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_upload_form_submit_upload($form, &$form_state) {
  $form_state['rebuild']             = TRUE;
  $form_state['storage']['method']   = 'upload';
  $form_state['storage']['selected'] = $form_state['input']['vod_infomaniak_upload_selectfolder'];

}


/**
 * Action on import from url - upload page.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_upload_form_submit_fromurl($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $api = vod_infomaniak_get_api();
  $option = array();
  if (empty($form_state['input']['vod_infomaniak_upload_fromurl_login']) === FALSE && empty($form_state['input']['vod_infomaniak_upload_fromurl_password']) === FALSE) {
    $option['login']    = $form_state['input']['vod_infomaniak_upload_fromurl_login'];
    $option['password'] = $form_state['input']['vod_infomaniak_upload_fromurl_password'];
  }

  $url                              = $form_state['input']['vod_infomaniak_upload_fromurl_selectprotocol'] . "://" . $form_state['input']['vod_infomaniak_upload_fromurl_address'];
  $folder                           = VodInfomaniakEasyvodDb::getFolder($form_state['storage']['selected']);
  $result_bool                      = $api->importFromUrl($folder->sPath, $url, $option);
  $form_state['storage']['bResult'] = $result_bool;
}


/**
 * Display players page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_players() {
  return drupal_get_form('vod_infomaniak_players_form');
}


/**
 * Display Form of players.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_players_form() {
  global $base_url;
  vod_infomaniak_use_template('player');

  if (vod_infomaniak_plugin_ready() === FALSE) {
    VodInfomaniakPlayerView::displayConfigError();
  }
  else {
    require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
    VodInfomaniakPlayerView::init(array(
      'iGroupe'     => VodInfomaniakEasyvodDb::getOption('vod_api_group'),
      'iService'    => VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice'),
      'base'        => $base_url,
      'module'      => drupal_get_path('module', 'vod_infomaniak'),
    ));

    $players     = VodInfomaniakEasyvodDb::getPlayers();
    $player_used = VodInfomaniakEasyvodDb::getOption('player');
    $options     = array();

    if (empty($players) === TRUE) {
      $options[] = t('No player available');
    }
    else {
      $selected = '';
      foreach ($players as $player) {
        if (empty($player_used) === FALSE && $player_used == $player->iPlayer) {
          VodInfomaniakEasyvodDb::updateOption('player', $player->iPlayer);

          $options[$player->iPlayer] = ucfirst($player->sName);
          $selected                  = $player->iPlayer;
        }

        $options[$player->iPlayer] = ucfirst($player->sName);
      }
    }

    VodInfomaniakPlayerView::registerPlayerJS();
    VodInfomaniakPlayerView::registerPlayerCSS();
    VodInfomaniakPlayerView::displayPlayerForm($players, $options, $selected);
  }

  return VodInfomaniakPlayerView::getFormHtml();

}


/**
 * Action on the validation of Form.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_players_form_submit($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';

  $player = VodInfomaniakEasyvodDb::getPlayer(intval($form_state['input']['selectplayer']));
  if (empty($player) === FALSE) {
    VodInfomaniakEasyvodDb::updateOption('player', $player->iPlayer);
    VodInfomaniakEasyvodDb::updateOption('width', $player->iWidth);
    VodInfomaniakEasyvodDb::updateOption('height', $player->iHeight);
  }
}


/**
 * Display page playlists.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_playlists() {
  return drupal_get_form('vod_infomaniak_playlists_form');

}


/**
 * Display Form playlists.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_playlists_form() {
  global $base_url;
  vod_infomaniak_use_template('playlist');

  if (vod_infomaniak_plugin_ready() === FALSE) {
    VodInfomaniakPlaylistView::displayConfigError();
  }
  else {
    require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';

    $api           = vod_infomaniak_get_api();
    $playlist_list = $api->getPlaylists();
    $playlist      = VodInfomaniakEasyvodDb::getPlaylists();

    if (empty($playlist_list) === FALSE && empty($playlist) === TRUE) {
      foreach ($playlist_list as $playlist) {
        VodInfomaniakEasyvodDb::insertPlaylist($playlist['iPlaylistCode'], $playlist['sPlaylistName'], $playlist['sPlaylistDescription'], $playlist['iTotal'], $playlist['sMode'], $playlist['dCreated'], $playlist['iTotalDuration']);
      }
    }

    VodInfomaniakPlaylistView::init(array(
      'iGroupe'     => VodInfomaniakEasyvodDb::getOption('vod_api_group'),
      'iService'    => VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice'),
      'base'        => $base_url,
      'module'      => drupal_get_path('module', 'vod_infomaniak'),
    ));

    VodInfomaniakPlaylistView::registerPlaylistJS();
    VodInfomaniakPlaylistView::displayPlaylistForm($playlist);
  }

  return VodInfomaniakPlaylistView::getFormHtml();

}


/**
 * Action on the validation of Form.
 *
 * @param string   $form
 *   Form
 * @param string   $form_state
 *   Form status
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_playlists_form_submit_post($form, &$form_state) {
  global $user;
  $playlist = VodInfomaniakEasyvodDb::getPlaylist(intval($form_state['input']['playlistcode']));

  if (empty($playlist) === FALSE) {
    $content    = '<div align="center" width="100%"><iframe frameborder="0" width="480" height="360" src="http://vod.infomaniak.com/iframe.php?player=' . VodInfomaniakEasyvodDb::getOption('player') . '&vod=' . VodInfomaniakEasyvodDb::getOption('vod_api_icodeservice') . '&playlist=' . $playlist->iPlaylistCode . '&autostart=0&loop=0"></iframe></div>';
    $node       = new stdClass();
    $node->type = "article";
    node_object_prepare($node);

    $node->title    = $playlist->sPlaylistName;
    $node->language = LANGUAGE_NONE;
    $node->uid      = $user->uid;
    $node->body[$node->language][0]['value']   = $content;
    $node->body[$node->language][0]['summary'] = text_summary($content);
    $node->body[$node->language][0]['format']  = 'full_html';

    $path       = 'node_created_on' . date('YmdHis');
    $node->path = array('alias' => $path);
    node_save($node);
    $form_state['redirect']  = 'node/' . $node->nid;
  }

}


/**
 * Displaying the help of admin page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_help_admin() {
  return drupal_get_form('vod_infomaniak_help_admin_form');

}


/**
 * Display Form of the help page.
 *
 * @return void
 *   Returns the html page
 */
function vod_infomaniak_help_admin_form() {
  vod_infomaniak_use_template('help');
  VodInfomaniakHelpView::displayHelpForm();
  return VodInfomaniakHelpView::getFormHtml();

}


/**
 * Loads a template class to use its methods.
 *
 * @param string   $name
 *   Class name
 *
 * @return void
 *   Execute the action
 */
function vod_infomaniak_use_template($name) {
  $name   = trim(strtolower($name));
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/templates/' . $name . '_view.php';

}


/**
 * Records in Drupal CSS files.
 *
 * @param string   $file
 *   Css file
 * @param string   $view
 *   View to use
 *
 * @return void
 *   Execute the action
 */
function vod_infomaniak_register_css($file, $view) {
  drupal_add_css($file, $view);

}


/**
 * Records in Drupal JS files.
 *
 * @param string   $file
 *   Js file
 * @param string   $view
 *   View to use
 *
 * @return void
 *   Execute the action
 */
function vod_infomaniak_register_js($file, $view) {
  drupal_add_js($file, $view);

}


/**
 * Detecting the configuration of the plugin.
 *
 * @return void
 *   Execute the action
 */
function vod_infomaniak_plugin_ready() {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/vod.api.inc';
  $api_connected = VodInfomaniakEasyvodDb::getOption('vod_api_connected');

  if (empty($api_connected) === FALSE && $api_connected == "on") {
    return TRUE;
  }

  return FALSE;

}


/**
 * Encrypts the text.
 *
 * @param string   $text
 *   Text to encrypt
 *
 * @return string
 *   Returns the text encrypted
 */
function vod_infomaniak_encrypt($text) {
  return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

}


/**
 * Decrypts the text.
 *
 * @param string   $text
 *   Text to decrypt
 *
 * @return string
 *   Return the text decrypted
 */
function vod_infomaniak_decrypt($text) {
  return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

}


/**
 * Creating a temporary key.
 *
 * @param string   $token
 *   Token
 * @param string   $video_name
 *   Video name
 *
 * @return string
 *   Return temporary key
 */
function vod_infomaniak_get_temporary_key($token, $video_name) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  $time_tmp = time() + intval(VodInfomaniakEasyvodDb::getOption('vod_api_servTime'));
  return md5($token . $video_name . ip_address() . date("YmdH", $time_tmp));

}


/**
 * Get API.
 *
 * @return void
 *   API Object
 */
function vod_infomaniak_get_api() {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/vod.api.inc';

  $password = vod_infomaniak_decrypt(VodInfomaniakEasyvodDb::getOption('vod_api_password'));
  return new VodInfomaniakApi(VodInfomaniakEasyvodDb::getOption('vod_api_login'), $password, VodInfomaniakEasyvodDb::getOption('vod_api_id'));

}


/**
 * References the last import.
 *
 * @return array
 *   Returns the last import
 */
function vod_infomaniak_get_last_import() {
  $api         = vod_infomaniak_get_api();
  $last_import = $api->getLastImportation();
  VodInfomaniakEasyvodDb::updateOption('vod_last_import', serialize($last_import));
  return vod_infomaniak_tab_last_upload($last_import);

}


/**
 * Html table references the last uploads.
 *
 * @param array   $last_import
 *   last import
 *
 * @return string
 *   Returns an html table with the latest uploads
 */
function vod_infomaniak_tab_last_upload($last_import) {
  global $base_url;
  $content = '';

  if (empty($last_import) === FALSE) {
    $content .= '<span id="tabImportRefresh" style="float:left; padding-right: 20px; padding-top: 20px; padding-bottom: 20px;"></span>';
    $content .= '<br/><table class="adminlist" style="width: 99%""><thead><tr>';
    $content .= '<th> ' . t('File') . ' </th><th style="width:150px;"> ' . t('Date') . ' </th><th> ' . t('Status') . ' </th><th> ' . t('Description') . ' </th>';
    $content .= '</tr></thead><tbody>';
    foreach ($last_import as $import) {
      $content .= '<tr>';
      $content .= '<td><label style="font-weight:normal;"><img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/videofile.png" style="vertical-align:middle"/> ' . $import["sFileName"] . '</label></td>';
      $content .= '<td>' . $import["dDateCreation"] . '</td>';
      $content .= '<td>';
      if ($import['sProcessState'] == "OK") {
        $content .= '<label> <img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/ico-tick.png" style="vertical-align:middle"/> ' . t('OK') . '</label>';
      }
      elseif ($import["sProcessState"] == "WARNING") {
        $content .= '<label> <img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/videofile.png" style="vertical-align:middle"/> ' . t('OK (alerts were triggered)') . '</label>';
      }
      elseif ($import["sProcessState"] == "DOWNLOAD") {
        $content .= '<label> <img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/ico-download.png" style="vertical-align:middle"/> ' . t('Downloading') . '</label>';
      }
      elseif ($import["sProcessState"] == "WAITING" || $import["sProcessState"] == "QUEUE" || $import["sProcessState"] == "PROCESSING") {
        $content .= '<label> <img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/ajax-loader.gif" style="vertical-align:middle"/> ' . t('Converting video') . '</label>';
      }
      else {
        $content .= '<label> <img src="' . $base_url . '/' . drupal_get_path("module", "vod_infomaniak") . '/images/ico-exclamation-yellow.png" style="vertical-align:middle"/> ' . t('Errors') . '</label>';
      }

      $content .= ' </td>';
      $content .= ' <td style="max-width:200px;">' . str_replace("'", "&#39", $import["sLog"]) . '</td>';
      $content .= '</tr>';
    }
    $content .= '</tbody></table>';
  }

  return $content;

}


/**
 * Quick sync.
 *
 * @param bool   $update_video
 *   Update videos
 *
 * @return bool
 *   True ou False
 */
function vod_infomaniak_fast_synchro($update_video = TRUE) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';

  $api_connected = VodInfomaniakEasyvodDb::getOption('vod_api_connected');
  if ($api_connected == "" || $api_connected == 'off' || $api_connected == NULL) {
    return FALSE;
  }

  $api         = vod_infomaniak_get_api();
  $last_update = VodInfomaniakEasyvodDb::getOption('vod_api_lastUpdate');

  if ($api->playerModifiedSince($last_update)) {
    VodInfomaniakEasyvodDb::cleanPlayers();
    $list_players = $api->getPlayers();

    if (empty($list_players) === FALSE) {
      foreach ($list_players as $player) {
        $player_used = VodInfomaniakEasyvodDb::getOption('player');
        if ($player_used == "" ||  $player_used == NULL) {
          VodInfomaniakEasyvodDb::updateOption('player', $player['iPlayerCode']);
        }
        elseif (VodInfomaniakEasyvodDb::getOption('player') == $player['iPlayerCode']) {
          VodInfomaniakEasyvodDb::updateOption('player', $player['iPlayerCode']);
          VodInfomaniakEasyvodDb::updateOption('width', $player['iWidth']);
          VodInfomaniakEasyvodDb::updateOption('height', $player['iHeight']);
        }

        VodInfomaniakEasyvodDb::insertPlayer($player['iPlayerCode'], $player['sName'], $player['iWidth'], $player['iHeight'], $player['bAutoStart'], $player['bLoop'], $player['dEdit'], $player['bSwitchQuality']);
      }
    }
  }

  if ($api->folderModifiedSince(VodInfomaniakEasyvodDb::getOption('vod_api_lastUpdate'))) {
    VodInfomaniakEasyvodDb::cleanFolders();
    $list_folders = $api->getFolders();

    if (empty($list_folders) === FALSE) {
      foreach ($list_folders as $folder) {
        VodInfomaniakEasyvodDb::insertFolder($folder['iFolderCode'], $folder['sFolderPath'], $folder['sFolderName'], $folder['sAccess'], $folder['sToken']);
      }
    }
  }

  $player_code = VodInfomaniakEasyvodDb::getOption('player');
  if (empty($player_code) === TRUE) {
    if (VodInfomaniakEasyvodDb::countPlaylists() != 0) {
      $default_player = VodInfomaniakEasyvodDb::getDefaultPlayerCode();
      VodInfomaniakEasyvodDb::insertOption('player', $default_player);
    }
    else {
      VodInfomaniakEasyvodDb::insertOption('player', 0);
    }
  }
  else {
    if ($api->playlistModifiedSince(VodInfomaniakEasyvodDb::getOption('vod_api_lastUpdate'))) {
      VodInfomaniakEasyvodDb::cleanPlaylists();
      $playlist_list = $api->getPlaylists();

      if (empty($playlist_list) === FALSE) {
        foreach ($playlist_list as $playlist) {
          VodInfomaniakEasyvodDb::insertPlaylist($playlist['iPlaylistCode'], $playlist['sPlaylistName'], $playlist['sPlaylistDescription'], $playlist['iTotal'], $playlist['sMode'], $playlist['dCreated'], $playlist['iTotalDuration']);
        }
      }
    }
  }

  if ($update_video) {
    $last_video = VodInfomaniakEasyvodDb::getLastVideo();
    if (empty($last_video) === FALSE) {
      $last_import_date = strtotime($last_video['dUpload']);
      $is_synchro       = FALSE;
      $current_page     = 0;

      while ($is_synchro === FALSE) {
        $videos = $api->getLastVideo(10, ($current_page * 10));
        $video_count = 0;
        while ($is_synchro === FALSE && $video_count < count($videos)) {
          $video = $videos[$video_count];
          if ($last_import_date < strtotime($video['dFileUpload'])) {
            VodInfomaniakEasyvodDb::insertVideo($video['iFileCode'], $video['iFolder'], $video['sFileName'], $video['sFileServerCode'], $video['aEncodes'][0]['sPath'], $video['aEncodes'][0]['eConteneur'], $video['fFileDuration'], $video['dFileUpload']);
            $video_count++;
          }
          else {
            $is_synchro = TRUE;
          }
        }
        $current_page++;
      }
    }
  }

  $processings = VodInfomaniakEasyvodDb::getUploadProcess();
  if (empty($processings) === FALSE) {
    $last_importation = $api->getLastImportation(50);

    foreach ($last_importation as $import) {
      if ($import['sProcessState'] == "OK" && empty($import['iVideo']) === FALSE && strpos($import['sInfo'], "drupal_upload_post_") !== FALSE) {
        foreach ($processings as $process) {
          if ("drupal_upload_post_" . $process->sToken == $import['sInfo']) {
            VodInfomaniakEasyvodDb::updateUpload($process->sToken, $import['iVideo']);
          }
        }
      }
    }
  }

  $server_time = $api->time();
  $local_time = time();
  $diff = ($server_time - $local_time);
  VodInfomaniakEasyvodDb::updateOption('vod_api_servTime', $diff);
  VodInfomaniakEasyvodDb::updateOption('vod_api_lastUpdate', time());
  VodInfomaniakEasyvodDb::updateOption('vod_count_playlist', VodInfomaniakEasyvodDb::countPlaylists());
  VodInfomaniakEasyvodDb::updateOption('vod_countVideo', VodInfomaniakEasyvodDb::countVideo());
  VodInfomaniakEasyvodDb::updateOption('vod_countPlayer', VodInfomaniakEasyvodDb::countPlayer());
  VodInfomaniakEasyvodDb::updateOption('vod_countFolder', VodInfomaniakEasyvodDb::countFolder());

  return TRUE;

}


/**
 * Full synchronization.
 *
 * @return bool
 *   True ou False
 */
function vod_infomaniak_full_synchro() {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'vod_infomaniak') . '/models/EasyVod_db.inc';
  $api_connected = VodInfomaniakEasyvodDb::getOption('vod_api_connected');

  if ($api_connected == "" || $api_connected == 'off' || $api_connected == NULL) {
    return FALSE;
  }

  $api = vod_infomaniak_get_api();
  vod_infomaniak_fast_synchro(FALSE);
  $max_video_per_page = 200;
  VodInfomaniakEasyvodDb::cleanVideos();
  $video_count = $api->countVideo();
  $page_total = floor(($video_count - 1) / $max_video_per_page);

  for ($current_page = 0; $current_page <= $page_total; $current_page++) {
    $videos = $api->getLastVideo($max_video_per_page, $current_page * $max_video_per_page);

    if (empty($videos) === FALSE) {
      foreach ($videos as $video) {
        VodInfomaniakEasyvodDb::insertVideo($video['iFileCode'], $video['iFolder'], $video['sFileName'], $video['sFileServerCode'], $video['aEncodes'][0]['sPath'], $video['aEncodes'][0]['eConteneur'], $video['fFileDuration'], $video['dFileUpload']);
      }
    }
  }

  return TRUE;

}
