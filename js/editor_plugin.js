/**
 * Grouping JS needed to run the plugin functions VOD infomaniak
 *
 * @author Capellaro Alexandre
 * @link http://statslive.infomaniak.ch/vod/api/
 * @version 1.0
 * @copyright infomaniak.ch
 *
 */


//Function to display the fieldset delete videos
var confirmVodDelete = function(iVideo, sTitle) {
  if (confirm("Supprimer définitivement la video " + sTitle + " ?")) {
    jQuery("#dialog-confirm-id").val(iVideo);
    jQuery("#dialog-confirm-title").val(sTitle);
    jQuery("#edit-submit-delete").click();
  } else {
    return false;
  }
};


//Function to display the fieldset modification and integration videos
var openVodPopup = function(iApi, sGroup, sCodeService, iVideo, title, url, sExtension, sAccess, sToken, iFolder) {
  jQuery(".pager").hide();
  jQuery("#customSearch").hide();
  var urlComplete = iApi + url;
  var sParam = "";
  if (sToken != "") {
    sParam = "?sKey=" + sToken;
  }
  jQuery("#dialog-modal-id").val(iVideo);
  jQuery("#dialog-post-id").val(iVideo);
  jQuery("#dialog-modal-title").text(title);
  jQuery("#edit-dialog-modal-name").val(title);
  jQuery("#dialog-modal-url").val("http://vod.infomaniak.com/redirect/" + urlComplete + "." + sExtension);
  jQuery("#dialog-modal-url-href").attr("href", "http://vod.infomaniak.com/redirect/" + urlComplete + "." + sExtension + sParam);
  jQuery("#dialog-modal-url-img").val("http://vod.infomaniak.com/redirect/" + urlComplete + ".jpg");
  jQuery("#dialog-modal-url-img-href").attr("href", "http://vod.infomaniak.com/redirect/" + urlComplete + ".jpg");
  jQuery("#dialog-modal-admin").attr("href", "https://statslive.infomaniak.com/vod/videoDetail.php/g" + sGroup + "s7i" + sCodeService + "?iFileCode=" + iVideo);
  jQuery("#dialog-modal-admin2").attr("href", "https://statslive.infomaniak.com/vod/videoDetail.php/g" + sGroup + "s7i" + sCodeService + "?iFileCode=" + iVideo + "&tab=3");
  jQuery("#dialog-modal-video").attr("src", "http://vod.infomaniak.com/iframe.php?url=" + urlComplete + "." + sExtension + sParam + "&player=576&vod=214&preloadImage=" + urlComplete + ".jpg");

  textAccess = "";
  if (sAccess != '' && sAccess != 'all') {
    textAccess += "Vidéo Géolocalisée";
  }

  if (sToken != "") {
    if (textAccess != "") {
      textAccess += ", ";
    }
    textAccess += "Securisé avec un token";
  }

  if (textAccess != "") {
    jQuery("#dialog-modal-access").text(textAccess);
    jQuery("#dialog-modal-access-block").show();
  } else {
    jQuery("#dialog-modal-access-block").hide();
  }

  jQuery("#edit-videotable2").hide();
  jQuery("#dialog-modal-vod").show();
  jQuery(".sticky-enabled").hide();
  return false;
};


//Function to close the "popup" vod
var closeVodPopup = function(url, drupalroot, folderFilter, drupalmodulepath, drupalbasepath) {
  jQuery("#dialog-modal-vod").hide();
  jQuery(".sticky-enabled").show();
  jQuery("#customSearch").show();

  var search = jQuery('#edit-input').val().trim();
  if(search == ""){
    jQuery(".pager").show();
  }else{
    jQuery(".pager").hide();
  }

  jQuery.ajax({
    type: "post",
    url: url,
    data: {recherche:search,drupalroot:drupalroot,folderFilter:folderFilter,drupalmodulepath:drupalmodulepath,drupalbasepath:drupalbasepath},
    success: function(html){
      jQuery("#edit-videotable2").html(html);
      jQuery("#edit-videotable").hide();
      jQuery("#edit-videotable2").show();
      jQuery('#edit-input').focus();
    }
  });
  return false;
};
