var showlist = function(){
  jQuery("#hiddenVideoFields").show();
  jQuery("#hidelist").show();
  jQuery("#showlist").hide();
};

var hidelist = function(){
  jQuery("#hiddenVideoFields").hide();
  jQuery("#hidelist").hide();
  jQuery("#showlist").show();
};

var showBlockOption = function(){
  if(jQuery("#optionsBlock").is(":visible")){
    jQuery("#optionsBlock").hide();
  }else{
    jQuery("#optionsBlock").show();
  }
};

var searchVideo = function(drupalroot, path_module, vod_filter_folder, vod_api_id){
  var sRecherche = jQuery("#recherche-input").val();
  jQuery.ajax({
        type: "post",
        url: path_module + "/ajax/videoSearchArticle.php",
        data: {recherche:sRecherche,drupalroot:drupalroot,folderFilter:vod_filter_folder,vod_api_id:vod_api_id},
        success: function(html){
          jQuery("#videolist").html(html);
        }
    });
};

var addVideo = function(iApi,url,sExtension,iPlayer, iDefaultPlayer, vod_api_icodeservice){
  jQuery("#edit-body-und-0-format--2").val("full_html");
  var urlComplete = iApi + url;
  var sToken      = "";
  var sParam      = "";

  if (sToken != "") {
    sParam  = "?sKey=" + sToken;
    sBalise = "vod tokenfolder='" + iFolder + "'";
  } else {
    sBalise = "vod";
  }

  iframeWidth = jQuery("#largeur").val();
  iframeHeight = jQuery("#hauteur").val();
  iframeAutoStart = 0;
  if ($("#autostart").is(":checked")) {
    iframeAutoStart = 1;
  }

  iframeAutoLoop = 0;
  if ($("#autoloop").is(":checked")) {
    iframeAutoLoop = 1;
  }

  if (iPlayer == 0) {
    iPlayer = parseInt(iDefaultPlayer, 10);
  }

  iframeUrl = "http://vod.infomaniak.com/iframe.php?url=" + urlComplete + "." + sExtension + sParam + "&preloadImage=" + urlComplete + ".jpg&player=" + iPlayer + "&vod=" + vod_api_icodeservice + "&autostart=" + iframeAutoStart + "&loop=" + iframeAutoLoop;
  jQuery("#edit-body-und-0-value").insertAtCaret("<iframe frameborder='0' width='" + iframeWidth + "' height='" + iframeHeight + "' src='" + iframeUrl + "'></iframe>");
};

jQuery.fn.extend({
  insertAtCaret: function(myValue) {
    return this.each(function(i) {
      if (document.selection) {
        this.focus();
        sel      = document.selection.createRange();
        sel.text = myValue;
        this.focus();
      }
      else if (this.selectionStart || this.selectionStart == "0") {
        var startPos        = this.selectionStart;
        var endPos          = this.selectionEnd;
        var scrollTop       = this.scrollTop;
        this.value          = this.value.substring(0, startPos) + myValue + this.value.substring(endPos,this.value.length);
        this.selectionStart = startPos + myValue.length;
        this.selectionEnd   = startPos + myValue.length;
        this.scrollTop      = scrollTop;
        this.focus();
      } else {
        this.value += myValue;
        this.focus();
      }
    })
  }

});
